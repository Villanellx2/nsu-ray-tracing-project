#include "tinyxml2.h"
#include <vector>
#include <map>
#include <stack>
#include <string>

using tinyxml2::XMLDocument;
using tinyxml2::XMLError;
using tinyxml2::XMLNode;
using tinyxml2::XMLElement;
using tinyxml2::XML_SUCCESS;

static int findAtribute2(float *arr, int waitingNumberArg, const char *atributeName, XMLElement *xmlElem) {
  const char *atribute = xmlElem->Attribute(atributeName);
  if (atribute == nullptr)
    return 0;
  char *pEnd;
  arr[0] = (float) strtod(atribute, &pEnd);
  int i = 1;
  float d;
  while (i < waitingNumberArg) {
    d = (float) strtod(pEnd, &pEnd);
    arr[i++] = (float) d;
  }
  return i;
}

class Materialx3d {
  public:
  float shininess;
  float diffuseColor[3] = {0.8, 0.8, 0.8};
  float transparency;
  float flag = 1;
  std::string path = "";
  float density = 1.6;
  float roughness = 0.0;
  float specular = 0;

  Materialx3d() {
    for (float &i : diffuseColor) {
      i = 0.8;
    }
    shininess = 0.2;
    transparency = 0;
  }
};

class Appearancex3d {
  public:
  Materialx3d material;
};

class Transformationx3d {
  public:
  float rotation[4]{};
  float scale[3]{};
  float translation[3]{};

  Transformationx3d() {
    for (int i = 0; i < 3; ++i) {
      rotation[i] = 0;
      scale[i] = 1;
      translation[i] = 0;
    }
    rotation[3] = 0;
    rotation[2] = 1;
  }
};

class type {
  public:
  std::string typeName;

  type() {
    typeName = "T_T";
  }
};

class Shapex3d {
  public:
  Appearancex3d *shapeAppearance;
  Transformationx3d shapeTrans;
  type *shapeType;
  int isLight = 0;
  float intensity = 0.2;

  Shapex3d() {
    shapeAppearance = new Appearancex3d();
    shapeType = new type();
  }
};

