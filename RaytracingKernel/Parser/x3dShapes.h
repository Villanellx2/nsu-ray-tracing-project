#include "x3dElems.h"
#include <vector>
#include <map>
#include <stack>
#include <string>

using tinyxml2::XMLDocument;
using tinyxml2::XMLError;
using tinyxml2::XMLNode;
using tinyxml2::XMLElement;
using tinyxml2::XML_SUCCESS;

class Spherex3d : public type {
public:
    float radius;

    explicit Spherex3d(XMLNode* Node) {
        typeName = "Sphere";
        radius = 1;
        if (Node != nullptr) {
            if (Node->ToElement()->Attribute("radius") != nullptr)
                radius = atof(Node->ToElement()->Attribute("radius"));
        }
    }

    Spherex3d() {
        typeName = "Sphere";
        radius = 1;
    }
};

class Boxx3d : public type {
public:
    float cord[3] = { 2,2,2 };

    Boxx3d(XMLNode* Node) {
        typeName = "Box";
        if (Node != nullptr) {
            if (findAtribute2(cord, 3, "size", Node->ToElement()) < 3) {
                cord[0] = cord[1] = cord[2] = 2;
            }
        }
    }
};

class Conex3d : public type {
public:
    float bottomRadius;
    float height;
    float topRadius;

    Conex3d() {
        typeName = "Cone";
        bottomRadius = 1.0;
        height = 2.0;
        topRadius = 0.0;

    }
};

class Cylinderx3d : public type {
public:
    float radius;
    float height;

    Cylinderx3d(XMLNode* Node) {
        typeName = "Cylinder";
        height = 2.0;
        radius = 1.0;
        if (Node != nullptr) {
            XMLElement* elem = Node->ToElement();
            if (findAtribute2(&height, 1, "height", elem) < 1) {
                height = 2.0;
            }
            if (findAtribute2(&radius, 1, "radius", elem) < 1) {
                radius = 1.0;
            }
        }
    }
};

class Planex3d : public type {
public:
    float center[3]{};
    float size[3]{};

    explicit Planex3d(XMLNode* Node) {
        typeName = "Plane";
        if (Node && Node->ToElement()) {
            XMLElement* elem = Node->ToElement();
            if (findAtribute2(center, 3, "center", elem) < 3)
                center[0] = center[1] = center[2] = 0;
            if (findAtribute2(size, 2, "size", elem) < 2)
                size[0] = size[1] = 2;
            size[2] = 0;
        }
        else {
            center[0] = center[1] = center[2] = 0;
            size[0] = size[1] = size[2] = 1;
        }
    }
};
