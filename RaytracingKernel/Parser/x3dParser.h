#include "x3dShapes.h"
#include "./../Kernel/RayTracer.h"
#include "./../Kernel//Geometry/Primitives/Box.h"
#include <vector>
#include <map>
#include <stack>
#include <string>
#include "tinyxml2.h"


class Parser {
public:
    /**
     * This method used to create parser and parse xml file.
     * @param name - path to xml file
     */
    Parser(const char* name) {
        skipHTML(name);
        int d = 0;
        std::stack<Transformationx3d> tab;
        createGetList();
    }
    /**
    * This method used to create parser and parse xml file.
    * @param name - path to xml file
    */
    Parser(std::string name) {
        char * name1 = (char*)malloc(sizeof(char) * name.length() + 1);
        strcpy(name1, name.c_str());
        skipHTML(name1);
        int d = 0;
        std::stack<Transformationx3d> tab;
        createGetList();
    }
    /**
    * @return true if you can take one more object
    */
    bool hasNext() {
        return !listToPop.empty();
    }
    /**
     * this method return next parsed object from xml file
     * @return shape taken from xml file.
     */
    BasicShape * NextElem() {
        BasicShape * s = new BasicShape();
        if (hasNext()) {
            s = listToPop.top();
            listToPop.pop();
        }
        return s;
    }

    /* This method return rotation of camera from viewpoint*/
    Quaternion getCameraRotation() {
        return camRot;
    }

    /* This method return position of camera from viewpoint*/
    Vector3 getCameraPosition() {
        return camPos;
    }


    /*This method return sourses of point light on scene
    vector can be empty!
    */
    std::vector<PointLight*> getSoursesOfLight() {
        return lights;
    }

    /*This method return sourses of volumetric light on the scene
    vector can be empty!
    */
    std::vector<VolumetricLight*> getVolumetricLight() {
        return vl;
    }

    /*Return global light from the scene, 
    can be equals to NULL if the is no GlobalLight on the scene
    */
    GlobalLight* getGlobalLight() {
        return global;
    }
    
    /*Return URL to sky texture;
    Parser didn't check that this texture exist
    */
    std::string getSky() {
        return skyURL;
    }

    /*Return true if xml file contain path to background texture;
    */
    bool hasSky() {
        return skyURL != "";
    }


private:
    GlobalLight* global = NULL;
    std::vector<VolumetricLight*> vl;
    std::vector<PointLight*> lights;
    std::stack<Shapex3d> list;
    std::stack<BasicShape*> listToPop;
    int pos = 0;
    std::map<std::string, Materialx3d> material_map;
    std::map<std::string, Appearancex3d> appearance_map;
    std::map<std::string, Transformationx3d> transformation_map;
    Quaternion camRot= Quaternion(0,0,1,0);
    Vector3 camPos = Vector3(0,-10,0);
    std::string skyURL;

    void skipHTML(const char* fileName) {
        XMLDocument xml_doc;
        XMLError eResult = xml_doc.LoadFile(fileName);
        if (eResult != XML_SUCCESS) {
            //  printf("Incorrect file!\n");
            return;
        }
        XMLNode* xml = xml_doc.FirstChild();
        if (xml == nullptr) {
            return;
        }
        while (strcmp(xml->Value(), "xml") != 0 && strcmp(xml->Value(), "html") != 0 && strcmp(xml->Value(), "x3d") != 0 &&
            strcmp(xml->Value(), "X3D") != 0) {
            xml = xml->NextSibling();
            if (xml == nullptr) {
                return;
            }
        }
        XMLNode* Node;
        if (strcmp(xml->Value(), "xml") == 0 || strcmp(xml->Value(), "html") == 0) {
            XMLNode* body;
            body = xml->FirstChildElement("body");
            if (body == nullptr) {
                return;
            }
            Node = body->FirstChildElement("X3D");
            if (Node == nullptr) {
                Node = body->FirstChildElement("x3d");
                if (Node == nullptr) {
                    return;
                }
            }
        }
        else {
            Node = xml;
        }
        
        Node = Node->FirstChildElement("scene");
        if (Node != nullptr) {
            createCamera(Node->FirstChildElement("viewpoint"));
            parseLight(Node->FirstChildElement("light"));
            parseSky(Node->FirstChildElement("background"));
            Node = Node->FirstChild();
        }
        if (Node != nullptr)
            NodeParser(Node);
        
    }
    
    void parseSky(XMLNode* skyNode) {
        if (skyNode == nullptr) {
            skyURL = "";
            return;
        }
        
        const char* name;
        XMLElement* Elem = skyNode->ToElement();
        if (Elem->Attribute("URL") != nullptr) {
            name = Elem->Attribute("URL");
            skyURL = name;
        }
        else {
            skyURL = "";
        }
    }

    void parseLight(XMLNode* light) {
        if (light != nullptr) {
            float color[3];
            float loc[3];
            float intencity[1];
            XMLNode* lightElem = light->FirstChildElement();
            while (lightElem != nullptr) {
                // take global lightning
                std::string nodeValue = lightElem->Value();
                if (nodeValue == "GlobalLight") {
                    if (findAtribute(color, 3, "color", lightElem->ToElement()) < 3) {
                        color[0] = color[1] = color[2] = 1;
                    }
                    if (findAtribute(intencity, 1, "intencity", lightElem->ToElement()) < 1) {
                        intencity[0] = 0.2;
                    }
                    global = (new GlobalLight(Vector3(color[0], color[1], color[2]), intencity[0]));
                }
                else if (nodeValue == "PointLight") {
                    if (findAtribute(color, 3, "color", lightElem->ToElement()) < 3) {
                        color[0] = color[1] = color[2] = 1;
                    }
                    if (findAtribute(intencity, 1, "intencity", lightElem->ToElement()) < 1) {
                        intencity[0] = 0.2;
                    }
                    if (findAtribute(loc, 3, "loc", lightElem->ToElement()) < 3) {}
                    else {
                        lights.push_back(new PointLight(Vector3(color[0], color[1], color[2]), intencity[0], Vector3(loc[0], loc[1], loc[2])));
                    }
                }
                lightElem = lightElem->NextSibling();
            }
        }
        printf("");

    }

    void NodeParser(XMLNode* Node) {
        if (strcmp(Node->Value(), "group") == 0)
            Node = Node->NextSibling();
        while (Node) {
            // printf("%s\n", Node->Value());
            if (strcmp(Node->Value(), "group") == 0)
                NodeParser(Node->FirstChild());
            else if (strcmp(Node->Value(), "transform") == 0) {
                TransformParser(nullptr, Node);
            }
            else if (strcmp(Node->Value(), "shape") == 0) {
                ShapeParser(nullptr, Node);
            }
            Node = Node->NextSibling();
        }
    }

    void createCamera(XMLNode *viewpoint) {
        if (viewpoint == nullptr) return;
        float or [4];
        float pos[3];
        
        if (findAtribute(or , 4, "orientation", viewpoint->ToElement()) < 4) {
            or [0] = or [1] = or [3] = 0;
            or [2] = 1;
        }
        camRot = Quaternion(or [0], or [1], or [2], or [3]);

        if (findAtribute(pos , 3, "position", viewpoint->ToElement()) < 3) {
            pos[0] = pos[2] = 0;
            pos[1] = 10;
        }
        else {
            float t = pos[1];
            pos[1] = pos[2];
            pos[2] = t;
        }
        pos[1] = -abs(pos[1]);
        camPos = Vector3(pos[0], pos[1], pos[2]);
    }

    //parse field transform
    void TransformParser(Transformationx3d* T, XMLNode* Node) {
        XMLElement* node = Node->ToElement();
        if (node == nullptr) {
            return;
        }
        int numArgs = 0;
        auto* newTrans = new Transformationx3d();
        if ((numArgs = findAtribute(newTrans->translation, 3, "translation", node)) == 0)
            for (float& i : newTrans->translation)
                i = 0;
        else if (numArgs != 3)
            for (float& i : newTrans->translation)
                i = 0;

        if ((numArgs = findAtribute(newTrans->rotation, 4, "rotation", node)) == 0) {
            for (float& i : newTrans->rotation)
                i = 0;
            newTrans->rotation[2] = -5;
        }

        if ((numArgs = findAtribute(newTrans->scale, 3, "scale", node)) == 0) {
            for (float& i : newTrans->scale)
                i = 1;
        }
        else if (numArgs != 3)
            for (float& i : newTrans->scale)
                i = 0;
        if (T != nullptr) {
            for (int i = 0; i < 3; ++i) {
                newTrans->rotation[i] += T->rotation[i];
                newTrans->translation[i] += T->translation[i];
            }
            for (int i = 0; i < 3; ++i) {
                newTrans->scale[i] += T->scale[i];
            }
        }
        XMLNode* subNode = Node->FirstChild();
        while (subNode != nullptr) {
            if (strcmp(subNode->Value(), "transform") == 0) {
                TransformParser(newTrans, subNode);
            }
            if (strcmp(subNode->Value(), "group") == 0) { /*Groupx3d(); continue;*/ }
            if (strcmp(subNode->Value(), "shape") == 0) {
                ShapeParser(newTrans, subNode);
            }
            subNode = subNode->NextSibling();
        }
        if (T != nullptr) {
            for (int i = 0; i < 3; ++i) {
                newTrans->rotation[i] -= T->rotation[i];
                newTrans->translation[i] -= T->translation[i];
            }
            for (int i = 0; i < 3; ++i) {
                newTrans->scale[i] -= T->scale[i];
            }
        }
    }

    // this function parse attribute atributeName and save it in arr
    static int findAtribute(float* arr, int waitingNumberArg, const char* atributeName, XMLElement* xmlElem) {
        const char* atribute = xmlElem->Attribute(atributeName);
        if (atribute == nullptr)
            return 0;
        char* pEnd;
        arr[0] = (float)strtod(atribute, &pEnd);
        int i = 1;
        float d;
        while (i < waitingNumberArg) {
            d = (float)strtod(pEnd, &pEnd);
            arr[i++] = (float)d;
        }
        return i;
    }

    void ShapeParser(Transformationx3d* t, XMLNode* Shape) {
        auto* newShape = new Shapex3d();
        XMLElement* Elem = Shape->ToElement();
        if (Elem->Attribute("isLight") != nullptr) {
            findAtribute((float*)&newShape->isLight, 1, "isLight", Elem);
            findAtribute(&newShape->intensity, 1, "intensity", Elem);

        }
        XMLNode* Node = Shape->FirstChild();
        if (Node == nullptr)
            return;
        if (t != nullptr) {
            newShape->shapeTrans = *t;
        }
        else {
            t = new Transformationx3d();
        }
        XMLNode* ShapeType;
        XMLNode* appearance;
        if ((ShapeType = Shape->FirstChildElement("Sphere")) != nullptr) {
            newShape->shapeType = new Spherex3d(Shape->FirstChildElement("Sphere"));
        }
        if ((ShapeType = Shape->FirstChildElement("sphere")) != nullptr) {
            newShape->shapeType = new Spherex3d(Shape->FirstChildElement("sphere"));
        }
        if ((ShapeType = Shape->FirstChildElement("plane")) != nullptr) {
            newShape->shapeType = new Planex3d(Shape->FirstChildElement("plane"));
        }
        if ((ShapeType = Shape->FirstChildElement("box")) != nullptr) {
            newShape->shapeType = new Boxx3d(Shape->FirstChildElement("box"));
        }
        if ((ShapeType = Shape->FirstChildElement("cylinder")) != nullptr) {
            newShape->shapeType = new Cylinderx3d(Shape->FirstChildElement("cylinder"));
        }
        if ((appearance = Shape->FirstChildElement("appearance")) != nullptr) {
            newShape->shapeAppearance = new Appearancex3d();
            AppearanceParser(newShape->shapeAppearance, appearance);
        }
        list.push(*newShape);
    }


    void AppearanceParser(Appearancex3d* a, XMLNode* Node) {
        if (a == nullptr) return;
        if (Node->FirstChildElement("material") != nullptr) {
            XMLElement* Elem = Node->FirstChildElement("material");
            const char* attribute = Elem->Attribute("diffuseColor");
            if (attribute != nullptr && attribute[0] == '#') {
                for (int i = 0; i < 3; i+=2) {
                    int num = 0;
                    if (attribute[i + 1] >= 'A' && attribute[i + 1] <= 'F')
                        num = (attribute[i + 1] - 'A') * 16;
                    else if (attribute[i + 1] >= 'a' && attribute[i + 1] <= 'f')
                        num = (attribute[i + 1] - 'a') * 16;
                    else num = (attribute[i + 1] - '0') * 16;

                    if (attribute[i + 2] >= 'A' && attribute[i + 2] <= 'F')
                        num += (attribute[i + 2] - 'A');
                    else if (attribute[i + 2] >= 'a' && attribute[i + 1] <= 'f')
                        num += (attribute[i + 2] - 'a');
                    else num += (attribute[i + 2] - '0');
                    a->material.diffuseColor[i/2] = (float)((float)num / 255);
                }
            }
            else if (attribute != nullptr) {
                findAtribute(a->material.diffuseColor, 3, "diffuseColor", Elem);
            }

            attribute = Elem->Attribute("shininess");
            if (attribute != nullptr)
                findAtribute(&(a->material.shininess), 1, "shininess", Elem);
           
            attribute = Elem->Attribute("transparency");
            if (attribute != nullptr)
                findAtribute(&(a->material.transparency), 1, "transparency", Elem);
            
            attribute = Elem->Attribute("flag");
            if (attribute != nullptr)
                findAtribute(&(a->material.flag), 1, "flag", Elem);

            attribute = Elem->Attribute("texture");
            const char* name;
            if (attribute != nullptr) {
                name = Elem->Attribute("texture");
                (a->material.path) = name;
            }

            attribute = Elem->Attribute("roughness");
            if (attribute != nullptr)
                findAtribute(&(a->material.roughness), 1, "roughness", Elem);

            attribute = Elem->Attribute("density");
            if (attribute != nullptr)
                findAtribute(&(a->material.density), 1, "density", Elem);

            attribute = Elem->Attribute("specular");
            if (attribute != nullptr)
                findAtribute(&(a->material.specular), 1, "specular", Elem);
        }
    }

    void createGetList() {
        while (!list.empty()) {
            Shapex3d Checked_Shape = list.top();
            list.pop();

            auto* scale = new Vector3(Checked_Shape.shapeTrans.scale[0], Checked_Shape.shapeTrans.scale[1],
                Checked_Shape.shapeTrans.scale[2]);
            auto* location = new Vector3(Checked_Shape.shapeTrans.translation[0],
                Checked_Shape.shapeTrans.translation[1],
                Checked_Shape.shapeTrans.translation[2]);
            Object* Obj;
            float red = (Checked_Shape.shapeAppearance->material.diffuseColor[0]);
            float green = (Checked_Shape.shapeAppearance->material.diffuseColor[1]);
            float blue = (Checked_Shape.shapeAppearance->material.diffuseColor[2]);
            auto* rot = Checked_Shape.shapeTrans.rotation;
            double length = sqrt(rot[0] * rot[0] + rot[1] * rot[1] + rot[2] * rot[2]);
            rot[0] /= length;
            rot[1] /= length;
            rot[2] /= length;
            auto* rotation = new Quaternion(Vector3(rot[0], rot[1], rot[2]), rot[3]);
            float transparensy = Checked_Shape.shapeAppearance->material.transparency;
            float shine = Checked_Shape.shapeAppearance->material.shininess;
            float refract = 1 - transparensy - shine;
            float roughness = Checked_Shape.shapeAppearance->material.roughness;
            float density = Checked_Shape.shapeAppearance->material.density;
            float specular = Checked_Shape.shapeAppearance->material.specular;

            Material mat;

            std::string path = Checked_Shape.shapeAppearance->material.path;

            mat = Material(Vector3(red, green, blue), Vector3(refract, shine, transparensy), roughness, density, specular);
            if (path != "") {
                mat.setTextureMapName(path);
            }

            BasicShape* ps;
            if (Checked_Shape.shapeType->typeName == "Sphere") {
                Spherex3d* ps;
                ps = (Spherex3d*)Checked_Shape.shapeType;
                Sphere *curr = (new Sphere(ps->radius, *location, *rotation, *scale));
                curr->setMaterial(mat);
                curr->rotate(*rotation);
                listToPop.push(curr);
                continue;
            }
            if (Checked_Shape.shapeType->typeName == "Box") {
                Boxx3d* ps;
                ps = (Boxx3d*)Checked_Shape.shapeType;
                Box *curr = new Box(ps->cord[0], ps->cord[1], ps->cord[2], *location, *rotation, *scale);
                curr->setMaterial(mat);
                listToPop.push(curr);
                continue;
            }
            if (Checked_Shape.shapeType->typeName == "Cylinder") {
                Cylinderx3d* ps;
                ps = (Cylinderx3d*)Checked_Shape.shapeType;
                listToPop.push(new Cylinder(ps->radius, ps->height, Vector3(Checked_Shape.shapeTrans.translation[0] - ps->height / 2,
                    Checked_Shape.shapeTrans.translation[1],
                    Checked_Shape.shapeTrans.translation[2] - ps->radius), *rotation, *scale, mat));
                continue;
            }
            if (Checked_Shape.shapeType->typeName == "Plane") {
                Planex3d* ps;
                ps = (Planex3d*)Checked_Shape.shapeType;
                Plane* curr = (new Plane(0, *location, *rotation, *scale));
                curr->setMaterial(mat);
                if (Checked_Shape.isLight <= 0) {
                    listToPop.push((BasicShape*)curr);
                }
                else {
                    vl.push_back(new PlaneLight(curr, 100, Vector3(red, green, blue), Checked_Shape.intensity));
                }
               
                continue;
            }
        }
    }
};
