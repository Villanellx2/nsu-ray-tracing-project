#ifndef PLANELIGHT
#define PLANELIGHT
#include "VolumetricLight.h"
class PlaneLight : public VolumetricLight
{
public:
	PlaneLight(BasicShape* shape, int n, Vector3 color, float intencity) : VolumetricLight(shape, n, color, intencity) {};
};

#endif