#include "./PointLight.h"

float PointLight::calculateDirectionalLight(const Vector3& pos, const Vector3& normal, int colorId) const
{
	//float len = (location - pos).magnitude();
	return max(0.0f, normal * (location - pos).normal()) * color.e[colorId]  /* (1 + len + len * len)*/;
}

float PointLight::calculateDistance(const Vector3& position) const
{
	return (location - position).d2();
}

float PointLight::calculateSpecularLight(const Vector3& pos, const Vector3& normal, float s, int colorId) const
{
	if (s < 1) return 0;
	float val = normal * (location - pos).normal();
	if (val <= 0) return 0;
	return powf(val, s) * color.e[colorId]; /* (1 + len + len * len)*/;
}