#ifndef POINTLIGHT
#define POINTLIGHT
#include "./LightObject.h"

class PointLight : public Light
{
public:
	PointLight(Vector3 color = Vector3(1.f, 1.f, 1.f), float intencity = 1.0f, Vector3 loc = Vector3(), Quaternion rot = Quaternion(), Vector3 scale = Vector3(1, 1, 1)) : Light(color, intencity, loc, rot, scale) 
	{
	};
	float calculateDirectionalLight(const Vector3& pos, const Vector3& normal, int colorId) const override;
	float calculateDistance(const Vector3& position) const override;
	float calculateSpecularLight(const Vector3& pos, const Vector3& normal, float s, int colorId) const override;
};

#endif