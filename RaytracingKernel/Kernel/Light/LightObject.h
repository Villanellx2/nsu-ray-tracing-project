#ifndef LIGHTOBJECT_H
#define LIGHTOBJECT_H
#include "./../Object.h"

class Light : public Object
{
protected:
	Vector3 color;
	float intencity;
public:
	Vector3 getColor() { return color; }
	float getIntencity() { return intencity; }
	Light(Vector3 color = Vector3(1.f, 1.f, 1.f), float intencity = 1.0f, Vector3 loc = Vector3(), Quaternion rot = Quaternion(), Vector3 scale = Vector3()) : Object(loc, rot, scale)
	{
		this->color = color * intencity;
		//this->intencity = intencity;
	}
	virtual float calculateDistance(const Vector3& point) const { return 0; };
	virtual float calculateDirectionalLight(const Vector3& pos, const Vector3& normal, int colorId) const { return 0; };
	virtual float calculateSpecularLight(const Vector3& pos, const Vector3& normal, float s, int colorId) const { return 0; };
};
#endif