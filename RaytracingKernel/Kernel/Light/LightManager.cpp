#include "LightManager.h"

float LightManager::gatherDiffuseLightAtPoint(Vector3 position, Vector3 normal, int id, ExtraInfo info)
{
	float result = 0;
	for (auto l : light)
	{
		Ray r = Ray::makeNormalizedRay(position, l->getLocation() - position);
		HitResult res;
		if (scene->castRay(res, r, id, info) && l->calculateDistance(position) > res.dist) {
			continue;
		}
		result += l->calculateDirectionalLight(position, normal, id);
	}
	for (auto v : volumetricLight)
	{
		//vector<PointLight> tmp = v->generateLight();
		PointLight p = PointLight(v->val.getColor(), v->val.getIntencity());
		for (int i = 0; i < v->N; i++)
		{
			v->nextPoint(&p);
			//std::cerr << p.getLocation().e[0] << " " << p.getLocation().e[1] << " " << p.getLocation().e[2] << std::endl;
			Ray r = Ray::makeNormalizedRay(position, p.getLocation() - position);
			HitResult res;
			if (scene->castRay(res, r, id, info) && p.calculateDistance(position) > res.dist) {
				continue;
			}
			result += p.calculateDirectionalLight(position, normal, id);
		}
	}
	return result;
}

float LightManager::gatherSpecularLightAtPoint(Vector3 position, Vector3 normal, float s, int id, ExtraInfo info)
{
	float result = 0;
	for (auto l : light)
	{
		Ray r = Ray::makeNormalizedRay(position, l->getLocation() - position);
		HitResult res;
		if (scene->castRay(res, r, id, info) && l->calculateDistance(position) > res.dist) {
			continue;
		}
		result += l->calculateSpecularLight(position, normal, s, id);
	}
	for (auto v : volumetricLight)
	{
		PointLight p = PointLight(v->val.getColor(), v->val.getIntencity());
		for (int i = 0; i < v->N; i++)
		{
			v->nextPoint(&p);
			Ray r = Ray::makeNormalizedRay(position, v->val.getLocation() - position);
			HitResult res;
			if (scene->castRay(res, r, id, info) && v->val.calculateDistance(position) > res.dist) {
				continue;
			}
			result += v->val.calculateSpecularLight(position, normal, s, id);
		}
	}
	return result;
}

float LightManager::fullLightAtPoint(Vector3 position, Vector3 normal, int id, int maxDepth, ExtraInfo info, float energy)
{
	float res = 0;
	res += gatherDiffuseLightAtPoint(position, normal, id, info);
	res += findIndirectIllumination(position, normal, id, maxDepth, info, energy);
	return res * energy;
}

CUDA_HD void LightManager::calculateLight(LightInfo & lInfo, Vector3 position, Vector3 normal, float s, int id, int maxDepth, ExtraInfo traceInfo)
{
	for (auto l : light)
	{
		Ray r = Ray::makeNormalizedRay(position, l->getLocation() - position);
		HitResult res;
		ExtraInfo info = traceInfo;
		if (scene->castRay(res, r, id, info) && l->calculateDistance(position) > res.dist) {
			continue;
		}
		lInfo.directLight += l->calculateDirectionalLight(position, normal, id);
		lInfo.specularLight += l->calculateSpecularLight(position, normal, s, id);
	}
	for (auto v : volumetricLight)
	{
		PointLight p = PointLight(v->val.getColor(), 1);
		for (int i = 0; i < v->N; i++)
		{
			v->nextPoint(&p);
			Ray r = Ray::makeNormalizedRay(position, p.getLocation() - position);
			HitResult res;
			ExtraInfo info = traceInfo;
			if (scene->castRay(res, r, id, info) && p.calculateDistance(position) > res.dist) {
				continue;
			}
			lInfo.directLight += p.calculateDirectionalLight(position, normal, id);
			lInfo.specularLight += p.calculateSpecularLight(position, normal, s, id);
		}
	}
	lInfo.globalIllumination = gatherGlobalIllumination(position, normal, id, min(2, maxDepth), traceInfo);
}

float LightManager::gatherGlobalIllumination(Vector3 position, Vector3 normal, int id, int maxDepth, ExtraInfo info, float energy)
{
	float res = 0;
	res += findIndirectIllumination(position, normal, id, maxDepth, info, energy);
	res += basicLight->calculateDirectionalLight(position, normal, id);
	return res;
}

CUDA_HD float LightManager::findIndirectIllumination(Vector3 position, Vector3 normal, int id, int maxDepth, ExtraInfo info, float energy)
{
	if (maxDepth == -1 || energy < 1e-4f) return 0;
	float res = 0;
	for (int i = 0; i < secondaryRays; i++)
	{
		Vector3 dir = RandomGenerator::getInstance().randV3(Vector3(-1, -1, -1), Vector3(1, 1, 1));
		dir = dir + dir * (1.f - dir * normal);
		Ray ray = Ray::makeNormalizedRay(position, dir);
		HitResult hit;
		ExtraInfo info1 = info;
		if (scene->castRay(hit, ray, id, info1))
		{
			if (hit.nNormal * ray.dir >= 0) hit.nNormal *= -1;
			SurfaceProps props = scene->getShapeByID(hit.id)->getMaterial()->getPropsByUV(hit.u, hit.v, id);
			res += fullLightAtPoint(hit.nPos + hit.nNormal * BIAS, hit.nNormal, id, maxDepth - 1, info1, energy * props.reflectionRate) / secondaryRays;
		}
	}
	return res;
}
