#include "./GlobalLight.h"

float GlobalLight::calculateDirectionalLight(const Vector3& pos, const Vector3& normal, int colorId) const
{
	return color.e[colorId];
}

float GlobalLight::calculateDistance(const Vector3 & position) const
{
	return 0.0f;
}
