#ifndef LIGHTMANAGER
#define LIGHTMANAGER
#include "./../Kernel.h"
#include "./../Vector3.h"
#include "./GlobalLight.h"
#include "./PointLight.h"
#include "./VolumetricLight.h"
#include "./../Scene/Scene.h"

struct LightInfo
{
	float directLight = 0;
	float specularLight = 0;
	float globalIllumination = 1;
};

class LightManager 
{
private:

protected:
	vector<PointLight*> light;
	vector<VolumetricLight*> volumetricLight;
	GlobalLight* basicLight;
	
	Scene* scene;

public:
	int secondaryRays = 2;
	void setLights(vector<PointLight*> lights) 
	{
		light = lights;
	}

	void setVolumeLights(vector<VolumetricLight*> lights) 
	{
		volumetricLight = lights;
	}
	/**
	* Clears the old global light and sets the new one
	*/
	void setGlobalLight(GlobalLight* gl) 
	{
		delete basicLight;
		basicLight = gl;
	}

	CUDA_H LightManager(Scene* scene)
	{
		this->scene = scene;
		this->basicLight = new GlobalLight(Vector3(1, 1, 1), 0.3f);
	}
	CUDA_HD virtual float gatherDiffuseLightAtPoint(Vector3 position, Vector3 normal, int id, ExtraInfo info);
	CUDA_HD virtual float gatherSpecularLightAtPoint(Vector3 position, Vector3 normal, float s, int id, ExtraInfo info);
	CUDA_HD virtual float gatherGlobalIllumination(Vector3 position, Vector3 normal, int id, int maxDepth, ExtraInfo info, float energy = 1);
	CUDA_HD float findIndirectIllumination(Vector3 position, Vector3 normal, int id, int maxDepth, ExtraInfo info, float energy = 1);
	CUDA_HD float fullLightAtPoint(Vector3 position, Vector3 normal, int id, int maxDepth, ExtraInfo info, float energy = 1);

	CUDA_HD virtual void calculateLight(LightInfo& lInfo, Vector3 position, Vector3 normal, float s, int id, int maxDepth, ExtraInfo traceInfo);
};

#endif