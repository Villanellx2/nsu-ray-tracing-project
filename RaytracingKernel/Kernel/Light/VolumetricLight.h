#ifndef VOLUMETRIC
#define VOLUMETRIC
#include "./PointLight.h"
#include "./../Geometry/BasicShape.h"

class VolumetricLight
{
	BasicShape* shape;
	float intencity;
	Vector3 color;
public:
	int N;
	PointLight val;
	
	VolumetricLight(BasicShape* shape, int n, Vector3 color, float intencity)
	{
		this->shape = shape;
		this->intencity = intencity;
		N = n;
		this->color = color;
		val = PointLight(color, intencity / N, shape->generatePointOnShape());
	}
	 
	void nextPoint(PointLight *p)
	{
		p->setLocation(shape->generatePointOnShape());
	}
	/*vector<PointLight>*/ void generateLight(PointLight (&tmp)[400])
	{
		//vector<PointLight> res;
		for (int i = 0; i < N; i++)
		{
			//res.push_back(PointLight(color, intencity / N, shape->generatePointOnShape()));
			tmp[i] = PointLight(color, intencity / N, shape->generatePointOnShape());
		}
		//return res;

	}

};
#endif