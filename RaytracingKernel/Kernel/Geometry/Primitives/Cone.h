#ifndef CONE_H
#define CONE_H
#include "./../BasicShape.h" 

class Cone : public BasicShape
{

public: 
	float height;
	float radius;
	float sqrRadius;
	float angle;

	CUDA_HD Cone(float rad = 0.5f, float h = 2.0f, Vector3 Loc = Vector3(), Quaternion Rot = Quaternion(), Vector3 Sc = Vector3(1, 1, 1), Material Mat = Material(Vector3(1, 0, 0))) : BasicShape(Mat, Loc, Rot, Sc)
	{
		radius = rad;
		height = h;
		sqrRadius = radius * radius;
		angle = height * frsqrt(sqrRadius + height * height);
	}
	CUDA_HD virtual bool hit(const Ray& r, HitResult& hitres, int colId)
	{
		return false;
	}

};

#endif // !CONE
