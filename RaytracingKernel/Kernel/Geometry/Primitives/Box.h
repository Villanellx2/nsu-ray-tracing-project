#ifndef BOX_H
#define BOX_H
#include "./../BasicShape.h"

class Box : public BasicShape
{
public:

	float A, B, C;

	CUDA_HD virtual void UpdateBB()
	{
		Vector3 p[8];
		p[0] = getLocation();
		p[1] = getLocation() + X * getScale().e[0] * A;
		p[2] = getLocation() + Y * getScale().e[1] * B;
		p[3] = getLocation() + Z * getScale().e[2] * C;
	
		p[4] = getLocation() + X * getScale().e[0] * A + Y * getScale().e[1] * B;
		p[5] = getLocation() + X * getScale().e[0] * A + Z * getScale().e[2] * C;
		p[6] = getLocation() + Y * getScale().e[1] * B + Z * getScale().e[2] * C;

		p[7] = getLocation() + X * getScale().e[0] * A + Y * getScale().e[1] * B + Z * getScale().e[2] * C;
		bb = p[0];

		for (int i = 1; i < 8; i++)
		{
			bb.join(p[i]);
		}
		//printf("Lower: (%f, %f, %f), Upper: (%f, %f, %f)\n", BoundingLower.e[0], BoundingLower.e[1], BoundingLower.e[2], BoundingUpper.e[0], BoundingUpper.e[1], BoundingUpper.e[2]);
	}

	CUDA_HD Box(float a = 1.0f, float b = 1.0f, float c = 1.0f, Vector3 Loc = Vector3(), Quaternion Rot = Quaternion(), Vector3 Sc = Vector3(1, 1, 1), Material Mat = Material(Vector3(0, 0, 1))) : BasicShape(Mat, Loc, Rot, Sc)
	{
		A = a;
		B = b;
		C = c;
		UpdateBB();
	}

	CUDA_HD virtual bool hit(const Ray& r, HitResult& hitres, int colId)
	{

		Ray r1 = localize(r);

		Vector3 upper = Vector3(A, B, C);
		Vector3 lower = Vector3(0, 0, 0);
		float tnear, tfar;
		__m128 d1 = _mm_sub_ps(upper.me, r.start.me);
		__m128 d2 = _mm_sub_ps(lower.me, r.start.me);

		d1 = _mm_div_ps(d1, r.dir.me);
		d2 = _mm_div_ps(d2, r.dir.me);

		__m128 fd1 = _mm_min_ps(d1, d2);
		__m128 fd2 = _mm_max_ps(d1, d2);
		tnear = max(fd1.m128_f32[0], max(fd1.m128_f32[1], fd1.m128_f32[2]));
		tfar = min(fd2.m128_f32[0], min(fd2.m128_f32[1], fd2.m128_f32[2]));

		if (tfar < 0 || tnear > tfar) return false;
		hitres.u = hitres.v = 0;
		hitres.nPos = r1.pointAtTime(tnear);
		if (hitres.nPos.e[0] == 0) hitres.nNormal = Vector3(-1, 0, 0);
		if (hitres.nPos.e[1] == 0) hitres.nNormal = Vector3(0, -1, 0);
		if (hitres.nPos.e[2] == 0) hitres.nNormal = Vector3(0, 0, -1);
		if (hitres.nPos.e[0] == A) hitres.nNormal = Vector3(1, 0, 0);
		if (hitres.nPos.e[1] == B) hitres.nNormal = Vector3(0, 1, 0);
		if (hitres.nPos.e[2] == C) hitres.nNormal = Vector3(0, 0, 1);
		globalize(hitres);
		hitres.dist = (hitres.nPos - r.start).d2();
		return true;
		/*
		float t[6];
		bool hit[6];
		t[0] = (C - r1.start.z()) / r1.dir.z();
		t[1] = (0 - r1.start.z()) / r1.dir.z();

		float x = 0, y = 0, z = 0;

		x = r1.start.x() + r1.dir.x() * t[0];
		y = r1.start.y() + r1.dir.y() * t[0];
		hit[0] = (0 < x && x < A && 0 < y && y < B);

		x = r1.start.x() + r1.dir.x() * t[1];
		y = r1.start.y() + r1.dir.y() * t[1];
		hit[1] = (0 < x && x < A && 0 < y && y < B);

		t[2] = (0 - r1.start.x()) / r1.dir.x();
		t[3] = (A - r1.start.x()) / r1.dir.x();

		z = r1.start.z() + r1.dir.z() * t[2];
		y = r1.start.y() + r1.dir.y() * t[2];
		hit[2] = (0 < z && z < C && 0 < y && y < B);

		z = r1.start.z() + r1.dir.z() * t[3];
		y = r1.start.y() + r1.dir.y() * t[3];
		hit[3] = (0 < z && z < C && 0 < y && y < B);

		t[4] = (0 - r1.start.y()) / r1.dir.y();
		t[5] = (B - r1.start.y()) / r1.dir.y();

		z = r1.start.z() + r1.dir.z() * t[4];
		x = r1.start.x() + r1.dir.x() * t[4];
		hit[4] = (0 < z && z < C && 0 < x && x < A);

		x = r1.start.x() + r1.dir.x() * t[5];
		z = r1.start.z() + r1.dir.z() * t[5];
		hit[5] = (0 < z && z < C && 0 < x && x < A);

		int e = -1;

		for (int j = 0; j < 6; ++j) {
			if (hit[e]) {
				e = j;
				break;
			}
		}
		for (int j = e; j < 6; ++j) {
			if (hit[j] && abs(t[j]) < abs(t[e]))
				e = j;
		}


		Vector3 nPos = r1.start + r1.dir * t[e];

		hitres.nPos = nPos;
		hitres.dist = (nPos - r.start).magnitude();
		hitres.u = 0;
		hitres.v = 0;

		switch (e)
		{
		case -1:
			return false;
		case 0: {
			Vector3 nNormal = Vector3(0, 0, 1);
			hitres.nNormal = nNormal;
			globalize(hitres);
			hitres.dist = (nPos - r.start).d2();
			return 1;
		}
		case 1:
		{
			Vector3 nNormal = Vector3(0, 0, -1);
			hitres.nNormal = nNormal;
			globalize(hitres);
			hitres.dist = (nPos - r.start).d2();
			return 1;
		}
		case 2: {
			Vector3 nNormal = Vector3(1, 0, 0);
			hitres.nNormal = nNormal;
			globalize(hitres);
			hitres.dist = (nPos - r.start).d2();
			return 1;
		}
		case 3:
		{
			Vector3 nNormal = Vector3(-1, 0, 0);
			hitres.nNormal = nNormal;
			globalize(hitres);
			hitres.dist = (nPos - r.start).d2();
			return 1;
		}
		case 4:
		{
			Vector3 nNormal = Vector3(0, -1, 0);
			hitres.nNormal = nNormal;
			globalize(hitres);
			hitres.dist = (nPos - r.start).d2();
			return 1;
		}
		case 5:
		{
			Vector3 nNormal = Vector3(-1, 1, 0);
			hitres.nNormal = nNormal;
			globalize(hitres);
			hitres.dist = (nPos - r.start).d2();
			return 1;
		}

		}
		*/
	}
	
};


#endif
