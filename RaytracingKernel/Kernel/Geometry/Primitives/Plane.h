#ifndef PLANE
#define PLANE
#include "./../BasicShape.h"
#include "./../../Vector3.h"
#include "./../../Quaternion.h"
#include "./../../Extras/Misc.h"

class Plane : public BasicShape
{
public:
	bool bIsInf;
	CUDA_HD virtual void updateBB() override
	{
		Vector3 p[8];
		//better not to use infinite plane
		if (bIsInf)
		{
			p[0] = getLocation() + Xn * MXV + Yn * MXV - Zn * HBIAS;
			p[1] = getLocation() + Xn * MXV - Yn * MXV - Zn * HBIAS;
			p[2] = getLocation() - Xn * MXV + Yn * MXV - Zn * HBIAS;
			p[3] = getLocation() - Xn * MXV - Yn * MXV - Zn * HBIAS;
			p[4] = getLocation() + Xn * MXV + Yn * MXV + Zn * HBIAS;
			p[5] = getLocation() + Xn * MXV - Yn * MXV + Zn * HBIAS;
			p[6] = getLocation() - Xn * MXV + Yn * MXV + Zn * HBIAS;
			p[7] = getLocation() - Xn * MXV - Yn * MXV + Zn * HBIAS;
		}
		else
		{
			p[0] = getLocation() + Xn * getScale().x() + Yn * getScale().y() - Zn * HBIAS;
			p[1] = getLocation() + Xn * getScale().x() - Yn * getScale().y() - Zn * HBIAS;
			p[2] = getLocation() - Xn * getScale().x() + Yn * getScale().y() - Zn * HBIAS;
			p[3] = getLocation() - Xn * getScale().x() - Yn * getScale().y() - Zn * HBIAS;
									
			p[6] = getLocation() - Xn * getScale().x() + Yn * getScale().y() + Zn * HBIAS;
			p[4] = getLocation() + Xn * getScale().x() + Yn * getScale().y() + Zn * HBIAS;
			p[5] = getLocation() + Xn * getScale().x() - Yn * getScale().y() + Zn * HBIAS;
			p[7] = getLocation() - Xn * getScale().x() - Yn * getScale().y() + Zn * HBIAS;
		}
		bb = p[0];

		for (int i = 1; i < 8; i++)
		{
			bb.join(p[i]);
		}
	}

	CUDA_HD Plane(bool isInf, Vector3 Loc = Vector3(), Quaternion Rot = Quaternion(), Vector3 Sc = Vector3(1, 1, 1)) : BasicShape(Material(), Loc, Rot, Sc) 
	{
		bIsInf = isInf;
		if (bIsInf) setScale(Vector3(MXV, MXV, 1));
		updateBB();
	};

	CUDA_HD virtual bool culling(const Ray& r) override
	{
		return (r.dir * Z >= 0);
	}
	CUDA_HD Vector3 generatePointOnShape() override 
	{
		/*Vector3 random = RandomGenerator::getInstance().randV();//Vector3::random();
		random |= getScale();
		return getLocation() + Vector3(Xc * random, Yc * random, Zc * random);*/
		return RandomGenerator::getInstance().randV3(-scale, scale) + location;
	}

	CUDA_HD virtual bool hit(const Ray& r, HitResult& hitres, int colId) override
	{
		//if (!__super::hit(r, hitres, colId)) return false;
		//if (culling(r)) return false;

		Ray r1 = localize(r);

		if (r1.dir.e[2] >= 0)
		{
			return false;
		}

		float t = -r1.start.e[2] / r1.dir.e[2];

		//if (t <= 0) return false;

		Vector3 nPos = r1.pointAtTime(t);
		if (!bIsInf)
		{
			for (int i = 0; i < 2; i++)
			{
				if (nPos.e[i] < -1 || nPos.e[i] > 1) return false;
			}
			hitres.u = 1 - (1 + nPos.e[1]) / 2;
			hitres.v = (1 + nPos.e[0]) / 2;
		}
		hitres.nPos = nPos;
		hitres.nNormal = Vector3(0, 0, 1);
		globalize(hitres);
		hitres.dist = (hitres.nPos - r.start).d2();
		return true;
	}
	CUDA_D void hit1()
	{
		printf("hello plane?");
	}
};

#endif