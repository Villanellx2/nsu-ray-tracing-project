#ifndef SPHERE
#define SPHERE
#include "./../BasicShape.h"

class Sphere : public BasicShape
{
	float radius;
	float sqrRadius;
public:

	CUDA_HD virtual void updateBB() override
	{
		Vector3 p[8];
		p[0] = getLocation() + Xn * getScale().x() * radius + Yn * getScale().y() * radius + Zn * getScale().z() * radius;
		p[1] = getLocation() + Xn * getScale().x() * radius + Yn * getScale().y() * radius - Zn * getScale().z() * radius;
		
		p[2] = getLocation() + Xn * getScale().x() * radius - Yn * getScale().y() * radius + Zn * getScale().z() * radius;
		p[3] = getLocation() + Xn * getScale().x() * radius - Yn * getScale().y() * radius - Zn * getScale().z() * radius;
		
		p[4] = getLocation() - Xn * getScale().x() * radius + Yn * getScale().y() * radius + Zn * getScale().z() * radius;
		p[5] = getLocation() - Xn * getScale().x() * radius + Yn * getScale().y() * radius - Zn * getScale().z() * radius;

		p[6] = getLocation() - Xn * getScale().x() * radius - Yn * getScale().y() * radius + Zn * getScale().z() * radius;
		p[7] = getLocation() - Xn * getScale().x() * radius - Yn * getScale().y() * radius - Zn * getScale().z() * radius;

		bb =  p[0];

		for (int i = 1; i < 8; i++)
		{
			bb.join(p[i]);
		}
	}

	CUDA_HD Sphere(float nRadius = 3.0f, Vector3 Loc = Vector3(), Quaternion Quat = Quaternion(), Vector3 Sc = Vector3(1, 1, 1), Material Mat = Material(Vector3(0, 1, 0))) : BasicShape(Mat, Loc, Quat, Sc)
	{
		radius = nRadius;
		sqrRadius = radius * radius;
		updateBB();
	}


	CUDA_HD virtual bool Culling(const Ray& r)
	{
		return false;
	}
	CUDA_HD size_t size() override { return sizeof(Sphere); }
	CUDA_HD virtual bool hit(const Ray& r, HitResult& hitres, int colId)
	{
		//if (!__super::hit(r, hitres, colId)) return false;
		//if (Culling(r)) return false;
		
		Ray r1 = localize(r);

		float b = (r1.dir * r1.start);

		float c = r1.start.d2() - sqrRadius;

		float d = b * b - c;

		if (d < 0) return false;

		float t1 = (-b - sqrt(d));
		float t2 = (-b + sqrt(d));
		
		if (t1 >= 0)
		{
			hitres.nPos = hitres.nNormal = r1.pointAtTime(t1);
			hitres.nNormal /= radius;
			hitres.u = asinf(hitres.nNormal.e[2]) / PI + 0.5f;//(hitres.nPos.e[2] / radius + 1) / 2;
			hitres.v = atan2f(hitres.nPos.e[1], hitres.nPos.e[0]) / PI2 + 0.5f; //(hitres.nPos.e[1] / radius + 1) / 2;

			globalize(hitres);

			hitres.dist = (hitres.nPos - r.start).d2();
			return true;
		}
		else if (t2 >= 0)
		{
			hitres.nPos = hitres.nNormal = r1.pointAtTime(t2);
			hitres.nNormal /= radius;
			hitres.u = asinf(hitres.nPos.e[2]) / PI + 0.5f;//(hitres.nPos.e[2] / radius + 1) / 2;
			hitres.v = atan2f(hitres.nPos.e[1], hitres.nPos.e[0]) / PI2 + 0.5f; //(hitres.nPos.e[1] / radius + 1) / 2;

			globalize(hitres);

			hitres.dist = (hitres.nPos - r.start).d2();
			return true;
		}
		return false;
		
	}
	CUDA_D void hit1()
	{
		printf("hello sphere?");
	}
};

#endif