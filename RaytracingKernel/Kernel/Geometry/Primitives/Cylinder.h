#ifndef CYLINDER_H
#define CYLINDER_H
#include "./../BasicShape.h"

class Cylinder : public BasicShape
{
public:
	float height;
	float radius;
	float sqrRadius;

	CUDA_HD virtual void updateBB() override
	{
		Vector3 p[8];
		p[0] = getLocation() + X * getScale().x() * radius + Y * getScale().y() * radius;
		p[1] = getLocation() + X * getScale().x() * radius + Y * getScale().y() * radius;

		p[2] = getLocation() + X * getScale().x() * radius - Y * getScale().y() * radius;
		p[3] = getLocation() + X * getScale().x() * radius - Y * getScale().y() * radius;

		p[4] = getLocation() - X * getScale().x() * radius + Y * getScale().y() * radius + Z * getScale().z() * height;
		p[5] = getLocation() - X * getScale().x() * radius + Y * getScale().y() * radius - Z * getScale().z() * height;

		p[6] = getLocation() - X * getScale().x() * radius - Y * getScale().y() * radius + Z * getScale().z() * height;
		p[7] = getLocation() - X * getScale().x() * radius - Y * getScale().y() * radius - Z * getScale().z() * height;

		bb = p[0];

		for (int i = 1; i < 8; i++)
		{
			bb.join(p[i]);
		}
		//printf("Lower: (%f, %f, %f), Upper: (%f, %f, %f)\n", BoundingLower.e[0], BoundingLower.e[1], BoundingLower.e[2], BoundingUpper.e[0], BoundingUpper.e[1], BoundingUpper.e[2]);
	}

	CUDA_HD Cylinder(float rad = 0.5f, float h = 2.0f, Vector3 Loc = Vector3(), Quaternion Rot = Quaternion(), Vector3 Sc = Vector3(1, 1, 1), Material Mat = Material(Vector3(0, 0, 1))) : BasicShape(Mat, Loc, Rot, Sc)
	{
		height = h;
		radius = rad;
		sqrRadius = rad * rad;
		updateBB();
	}

	CUDA_HD virtual bool hit(const Ray& r, HitResult& hitres, int colId)
	{
		
		Ray r1 = localize(r);

		float a = r1.dir.e[0] * r1.dir.e[0] + r1.dir.e[1] * r1.dir.e[1];

		float b = 2 * (r1.dir.e[0] * r1.start.e[0] + r1.dir.e[1] * r1.start.e[1]);

		float c = r1.start.e[0] * r1.start.e[0] + r1.start.e[1] * r1.start.e[1] - sqrRadius;

		float d = b * b - 4 * a * c;

		//printf("a: %f, b: %f, c: %f\n", a, b, c);

		if (a == 0)
		{
			return false;
		}

		if (d < 0)
		{
			return false;
		}
		else
		{
			float d1 = sqrt(d);
			float t1 = (-b - d1) / 2.0f / a;
			float t2 = (-b + d1) / 2.0f / a;

			if (t1 >= 0)
			{
				//printf("time1: %f\n", t1);
				hitres.nPos = hitres.nNormal = r1.pointAtTime(t1);
				hitres.u = atan2(hitres.nPos.e[1], hitres.nPos.e[0]);
				hitres.v = hitres.nPos.e[2] / height;
				//printf("E-boi: %f\n", nPos.e[2]);
				if (!(0 <= hitres.nPos.e[2] && hitres.nPos.e[2] <= height)) return false;

				hitres.nNormal.e[2] = 0;

				globalize(hitres);


				hitres.dist = sqrt((hitres.nPos - r.start).d2());
				//printf("dist: %f\n", hitres.dist);
				return 1;
			}
			else if (t2 >= 0)
			{
				hitres.nPos = hitres.nNormal = r1.pointAtTime(t2);
				hitres.u = atan2(hitres.nPos.e[1], hitres.nPos.e[0]);
				hitres.v = hitres.nPos.e[2] / height;
				//printf("E-boi: %f\n", nPos.e[2]);
				if (!(0 <= hitres.nPos.e[2] && hitres.nPos.e[2] <= height)) return false;

				hitres.nNormal.e[2] = 0;

				globalize(hitres);


				hitres.dist = sqrt((hitres.nPos - r.start).d2());
				//printf("dist: %f\n", hitres.dist);
				return 1;
			}
			return 0;
		}
	}
};

#endif
