#ifndef MAT_H
#define MAT_H

#include "./../../Kernel.h"
#include "./../../Vector3.h"
#include "./MapManager.h"

struct SurfaceProps {
	float color;  
	float diffuseRate; 
	float reflectionRate; 
	float refractionRate; 
	float density; 
	float smoothness; 
	float specularRate; 
};

class Material
{
private:

	float density = 1.6f;
	Vector3 coefs; // diffuse, reflection, refreaction
	float roughness; // 0 - 1
	Vector3 color = Vector3(0.23f, 0.46f, 0.69f);
	float specularity = 0;

	string maps[10]; 

	/*
	* Uses the same order as in Surface props. tested only color map.
	*/
	uint32_t FLAGS; 

public:
	CUDA_HD Material(Vector3 colors = Vector3(0.1f, 1.0f, .3f), Vector3 coefs = Vector3(0.8f, 0.1f, 0.1f), float roughness = 0.0f, float density = 1.6f, float specular = 0)
	{
		this->color = colors;
		this->roughness = roughness;
		this->coefs = coefs;
		this->density = density;
		this->FLAGS = 0;
		this->specularity = specular;
	}
private: 

	CUDA_HD bool UVtoValue(float& value, int mapId, float u, float v, int id) {
		if (mapId < 0 || mapId >= 10) return false;
		if (!((FLAGS >> mapId) & 1) || maps[mapId] == "") return false;
		value = MapManager::getInstance().getTextureByName(maps[mapId])->colorByUV(u, v, id);
		return true;
	}

	CUDA_HD float UVtoColor(float u, float v, int id)
	{
		float value;

		if (UVtoValue(value, 0, u, v, id)) return value;
		else return color.e[id];
	}

	CUDA_HD float UVtoDiffuse(float u, float v, int id)
	{
		float value;
		if (UVtoValue(value, 1, u, v, id)) return value;
		else return coefs.x();
	}

	CUDA_HD float UVtoReflect(float u, float v, int id)
	{
		float value;
		if (UVtoValue(value, 2, u, v, id)) return value;
		else return coefs.y();
	}

	CUDA_HD float UVtoRefract(float u, float v, int id)
	{
		float value;
		if (UVtoValue(value, 3, u, v, id)) return value;
		else return coefs.z();
	}

	CUDA_HD float UVtoDensity(float u, float v, int id)
	{
		float value;
		if (UVtoValue(value, 4, u, v, id)) return 0.5f + value;
		else return density;
	}

	CUDA_HD float UVtoRoughness(float u, float v, int id)
	{
		float value;
		if (UVtoValue(value, 5, u, v, id)) return value;
		else return roughness;
	}

	CUDA_HD float UVtoSmoothness(float u, float v, int id) { return 1.0f - UVtoRoughness(u, v, id); }

	CUDA_HD float UVtoSpecularity(float u, float v, int id)
	{
		float value;
		if (UVtoValue(value, 6, u, v, id)) return value;
		else return specularity;
	}

	CUDA_H void setMapNameAndID(string mapName, int idx)
	{
		maps[idx] = mapName;
		FLAGS |= (1 << idx) * (mapName != "");
	}

	CUDA_H void setUseMap(bool val, int idx)
	{
		FLAGS &= ~(1 << idx);
		FLAGS |= (1 << idx) * (maps[idx] != "");
	}
public:
	CUDA_H void setTextureMapName(string mapName) { setMapNameAndID(mapName, 0); }
	CUDA_H void setUseTextureMapName(bool bIsUse) { setUseMap(bIsUse, 0); }

	CUDA_H void setDiffuseMapName(string mapName) { setMapNameAndID(mapName, 1); }
	CUDA_H void setUseDiffuseMapName(bool bIsUse) { setUseMap(bIsUse, 1); }

	CUDA_H void setReflectMapName(string mapName) { setMapNameAndID(mapName, 2); }
	CUDA_H void setUseReflectMapName(bool bIsUse) { setUseMap(bIsUse, 2); }

	CUDA_H void setRefractMapName(string mapName) { setMapNameAndID(mapName, 3); }
	CUDA_H void setUseRefractMapName(bool bIsUse) { setUseMap(bIsUse, 3); }

	CUDA_H void setDensityMapName(string mapName) { setMapNameAndID(mapName, 4); }
	CUDA_H void setUseDensityMapName(bool bIsUse) { setUseMap(bIsUse, 4); }

	CUDA_H void setRoughnessMapName(string mapName) { setMapNameAndID(mapName, 5); }
	CUDA_H void setUseRoughnessMapName(bool bIsUse) { setUseMap(bIsUse, 5); }

	CUDA_H void setSpecularMapName(string mapName) { setMapNameAndID(mapName, 6); }
	CUDA_H void setUseSpecularMapName(bool bIsUse) { setUseMap(bIsUse, 6); }

public:
	// get properties at point by coords
	CUDA_HD SurfaceProps getPropsByUV(float u, float v, int id) 
	{
		SurfaceProps res;
		res.color = UVtoColor(u, v, id);
		res.density = UVtoDensity(u, v, id);
		res.diffuseRate = UVtoDiffuse(u, v, id);
		res.reflectionRate = UVtoReflect(u, v, id);
		res.refractionRate = UVtoRefract(u, v, id);
		res.smoothness = UVtoSmoothness(u, v, id);
		res.specularRate = UVtoSpecularity(u, v, id);
		return res;
	}

	//load active maps
	CUDA_H void loadMaps()
	{
		for (int i = 0; i < 10; i++)
		{
			if (FLAGS & (1 << i))
				MapManager::getInstance().getTextureByName(maps[i]);
		}
	}
};

#endif