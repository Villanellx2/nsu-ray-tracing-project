#include "./Map.h"


float TextureMap::colorByUV(float u, float v, size_t offset)
{
	size_t id = channels * (uint32_t(max(0, min(u, 1 - EPS)) * height) * width + uint32_t(max(0, min(v, 1 - EPS)) * width));
	return (*(map + id + offset)) * 0.004f;
}