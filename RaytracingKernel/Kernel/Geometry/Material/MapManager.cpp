#include "./MapManager.h"

TextureMap * MapManager::loadTexture(string name)
{
	TextureMap* res = new TextureMap(name);
	if (!loadMap(name, &res))
	{
		delete res;
		res = new TextureMap(Vector3(0.5f, 0.1f, 0.8f));
	}
	return res;
}

TextureMap* MapManager::getTextureByName(string name)
{
	if (!maps.count(name))
	{
		maps[name] = loadTexture(name);
	}
	return maps[name];
}


bool MapManager::loadMap(string name, TextureMap** texture)
{
	string format;
	if (!getFormat(name, format))
	{
		return 0;
	}
	if (format == "ppm")
	{
		FILE* f;
		if (fopen_s(&f, name.data(), "r") != 0) return false;
		char info[2];
		uint32_t t;
		uint32_t width, height;
		fscanf_s(f, "%c%c%d%d%d\n", info, 1, info + 1, 1, &width, &height, &t);

		fclose(f);
		if (fopen_s(&f, name.data(), "rb") != 0) return false;
		fseek(f, 17, 0);
		uint8_t* map = (uint8_t*)malloc(height * width * 3 * sizeof(uint8_t));
		fread(map, sizeof(uint8_t), height * width * 3, f);
		fclose(f);
		(*texture)->setMap(map);
		(*texture)->setLoad(1);
		(*texture)->setName(name);
		(*texture)->setSize(height, width);
		return true;
	}
	else
	{
		return false;
	}
}