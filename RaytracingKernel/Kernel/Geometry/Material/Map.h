#ifndef MAP_STRUCT
#define MAP_STRUCT
#include "./../../Kernel.h"
#include "./../../Vector3.h"
#include "./../../Extras/Misc.h"

class TextureMap
{
	uint8_t *map;
	uint32_t height = 1;
	uint32_t width = 1;

	size_t channels;

	string name;
	bool bIsLoaded;

public:

	void setMap(uint8_t *nmap)
	{
		map = nmap;
	}

	void setSize(uint32_t nh, uint32_t nw)
	{
		height = nh;
		width = nw;
	}

	void setName(string nname)
	{
		name = nname;
	}

	void setLoad(bool val)
	{
		bIsLoaded = val;
	}

	TextureMap(string sName, size_t nChannels = 3)
	{
		name = sName;
		bIsLoaded = false;
		channels = nChannels;
	}
	
	TextureMap(Vector3 color, size_t nChannels = 3)
	{
		name = "Colored";
		map = (uint8_t*)malloc(3 * sizeof(uint8_t));
		map[0] = uint8_t(color.r() * 255.9);
		map[1] = uint8_t(color.g() * 255.9);
		map[2] = uint8_t(color.b() * 255.9);
		height = width = 1;
		bIsLoaded = true;
		channels = nChannels;
	}
	
	~TextureMap()
	{
		if (bIsLoaded) free(map);
	}


	float colorByUV(float u, float v, size_t offset);

};

#endif