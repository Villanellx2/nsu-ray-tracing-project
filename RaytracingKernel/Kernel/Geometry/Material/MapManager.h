#ifndef MAPMANAGER
#define MAPMANAGER
#include "./../../Kernel.h"
#include "./Map.h"

class MapManager
{
private:
	CUDA_H MapManager()
	{

	}
	CUDA_H ~MapManager()
	{

	}

public:
	std::unordered_map<string, TextureMap*> maps;

	TextureMap * loadTexture(string name);

	TextureMap * getTextureByName(string name);
	bool loadMap(string name, TextureMap** texture);

	static MapManager& getInstance()
	{
		static MapManager manager;
		return manager;
	}
};

#endif