#ifndef SHAPE_H
#define SHAPE_H

#include "./../Kernel.h"
#include "./../Object.h"
#include "./Material/Material.h"
#include "./../Ray.h"
#include "./../Extras/Misc.h"

class BasicShape : public Object
{
protected:
	bool isAlive;
	uint32_t link;
	Material mat;
public:
	int32_t link1 = -1;

	CUDA_H void setMaterial(Material material)
	{
		mat = material;
	}
	CUDA_H Material* getMaterial()
	{
		return &mat;
	}

	CUDA_HD bool getIsAlive()
	{
		return isAlive;
	}

	CUDA_HD uint32_t getListNode()
	{
		return link;
	}

	CUDA_HD void setListNode(uint32_t nlink)
	{
		link = nlink;
	}

	CUDA_HD BasicShape(Material Mat1 = Material(), Vector3 Loc = Vector3(), Quaternion Rot = Quaternion(), Vector3 Scal = Vector3()) : Object(Loc, Rot, Scal)
	{
		mat = Mat1;
		link = -1;
	}

	CUDA_HD virtual Vector3 generatePointOnShape() { return Vector3(0, 0, 0); }

	CUDA_HD virtual bool hit(const Ray& r, HitResult& hitres, int colId) { return bAlive; }

	CUDA_HD virtual bool culling(const Ray& r) { return false; }

	CUDA_HD virtual size_t size() { return sizeof(BasicShape); }

	CUDA_HD void kill() { isAlive = 0; }
};
#endif