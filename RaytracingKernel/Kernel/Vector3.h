#ifndef VECTOR_H
#define VECTOR_H
#include "./Kernel.h"

CUDA_HD inline float frsqrt(float num)
{
	float x2 = num * 0.5F;
	float threehalfs = 1.5F;
	union
	{
		float f;
		unsigned int i;
	} conv = { num };

	conv.i = 0x5f3759df - (conv.i >> 1);
	conv.f *= (threehalfs - (x2 * conv.f * conv.f));
	return conv.f;
}


struct Vector3
{
	union
	{
		float e[4] = {0, 0, 0, 0};
		__m128 me;
	};
	CUDA_HD inline float x() const { return e[0]; };
	CUDA_HD inline float y() const { return e[1]; };
	CUDA_HD inline float z() const { return e[2]; };
	CUDA_HD inline float r() const { return e[0]; };
	CUDA_HD inline float g() const { return e[1]; };
	CUDA_HD inline float b() const { return e[2]; };
	
	CUDA_HD inline float getByID(int id) const { return e[id]; }

	CUDA_HD inline Vector3(float x = 0, float y = 0, float z = 0) { e[0] = x; e[1] = y, e[2] = z; }
	CUDA_H inline Vector3(__m128 ve) { me = ve; }

	CUDA_HD inline Vector3 operator+(const Vector3 b) const 
	{
#ifdef __CUDA_ARCH__
		return Vector3(e[0] + b.e[0], e[1] + b.e[1], e[2] + b.e[2]); 
#else
		return Vector3(_mm_add_ps(me, b.me));
#endif
	}
	CUDA_HD inline Vector3 operator-(const Vector3 b) const {
#ifdef __CUDA_ARCH__
		return Vector3(e[0] - b.e[0], e[1] - b.e[1], e[2] - b.e[2]);
#else
		return Vector3(_mm_sub_ps(me, b.me));
#endif
	}
	CUDA_HD inline Vector3 operator*(const float b)   const 
	{ 
#ifdef __CUDA_ARCH__	
		return Vector3(e[0] * b, e[1] * b, e[2] * b);
#else
		return Vector3(_mm_mul_ps(me, _mm_set1_ps(b)));
#endif
	}
	CUDA_HD inline Vector3 operator-() const
	{
#ifdef __CUDA_ARCH__
#else
		return Vector3(_mm_mul_ps(me, _mm_set1_ps(-1)));
#endif
	}
	CUDA_HD inline Vector3 operator/(const float b)   const 
	{
#ifdef __CUDA_ARCH__
		return Vector3(e[0] / b, e[1] / b, e[2] / b); 
#else
		return Vector3(_mm_div_ps(me, _mm_set1_ps(b)));
#endif
	}
	CUDA_HD inline Vector3 operator|(const Vector3& b)   const
	{
#ifdef __CUDA_ARCH__
		return Vector3(e[0] * b.e[0], e[1] * b.e[1], e[2] * b.e[2]);
#else
		return Vector3(_mm_mul_ps(me, b.me));
#endif
	}
	CUDA_HD inline Vector3 operator/(const Vector3 b) const 
	{
#ifdef __CUDA_ARCH__
		return Vector3(e[0] / b.e[0], e[1] / b.e[1], e[2] / b.e[2]);
#else
		return Vector3(_mm_div_ps(me, b.me));
#endif
	}
	CUDA_HD inline float operator*(const Vector3& b)   const 
	{
#ifdef __CUDA_ARCH__
		return e[0] * b.e[0] + e[1] * b.e[1] + e[2] * b.e[2]; 
#else
		__m128 dp = _mm_dp_ps(me, b.me, 0x7F);

		float result;
		_mm_store_ss(&result, dp);

		return result;
		/*__m128 tmp = _mm_mul_ps(me, b.me);
		//__m128 res = _mm_dp_ps(me, b.me, 0x7F);
		return /*_mm_dp_ps(me, b.me, 0x7F).m128_f32[0];// tmp.m128_f32[0] + tmp.m128_f32[1] + tmp.m128_f32[2];*/
#endif
	}
	CUDA_HD inline Vector3 operator^(const Vector3& b) const { return Vector3(e[1] * b.e[2] - e[2] * b.e[1], b.e[0] * e[2] - e[0] * b.e[2], e[0] * b.e[1] - e[1] * b.e[0]); }
	
	CUDA_HD inline Vector3 operator+=(const Vector3& b) 
	{ 
#ifdef __CUDA_ARCH__
		e[0] += b.e[0];
		e[1] += b.e[1];
		e[2] += b.e[2];
#else
		me = _mm_add_ps(me, b.me);
#endif
		return *this;
	}

	CUDA_HD inline Vector3 operator-=(const Vector3& b)
	{
#ifdef __CUDA_ARCH__
		e[0] -= b.e[0];
		e[1] -= b.e[1];
		e[2] -= b.e[2];
#else
		me = _mm_sub_ps(me, b.me);
#endif
		return *this;
	}

	CUDA_HD inline Vector3 operator*=(const float b) 
	{
#ifdef __CUDA_ARCH__
		e[0] *= b;
		e[1] *= b;
		e[2] *= b;
#else
		me = _mm_mul_ps(me, _mm_set1_ps(b));
#endif
		return *this;
	}
	CUDA_HD inline Vector3 operator|=(const Vector3& b)
	{
#ifdef __CUDA_ARCH__
		e[0] *= b;
		e[1] *= b;
		e[2] *= b;
#else
		me = _mm_mul_ps(me, b.me);
#endif
		return *this;
	}
	CUDA_HD inline Vector3 operator/=(const float b)
	{
#ifdef __CUDA_ARCH__
		e[0] /= b;
		e[1] /= b;
		e[2] /= b;
#else
		me = _mm_div_ps(me, _mm_set1_ps(b));
#endif	
		return *this;
	}
	CUDA_HD inline Vector3 operator/=(const Vector3& b)
	{
#ifdef __CUDA_ARCH__
		e[0] /= b;
		e[1] /= b;
		e[2] /= b;
#else
		me = _mm_div_ps(me, b.me);
#endif	
		return *this;
	}
	// Squared magnitude
	CUDA_HD inline float d2() const
	{
		return (*this) * (*this);
	}
	// Vector's magnitude
	CUDA_HD inline float magnitude() const
	{
		float v = d2();
		
		//return v * frsqrt(v);
		return sqrt(v);
	}
	// Get normalized vector to given
	CUDA_HD inline Vector3 normal() const
	{
		/*float invlen = frsqrt(d2());
		return Vector3(e[0] * invlen, e[1] * invlen, e[2] * invlen);*/
#ifdef __CUDA_ARCH__
		float len = magnitude();
		return Vector3(e[0] / len, e[1] / len, e[2] / len);
#else
		__m128 dp = _mm_dp_ps(me, me, 0x7f);

		// compute rsqrt of the dot product
		dp = _mm_sqrt_ps(dp);
		//assert(abs(1.f - _mm_dp_ps(_mm_mul_ps(me, dp), _mm_mul_ps(me, dp), 0x7F).m128_f32[0]) < 1e-6);
		return Vector3(_mm_div_ps(me, dp));
#endif
	}
	// Normalize vector
	CUDA_HD inline void normalize()
	{
#ifdef __CUDA_ARCH__
		float invlen = frsqrt(d2());
		e[0] *= invlen;
		e[1] *= invlen;
		e[2] *= invlen;
#else
	/*	float len = magnitude();
		me = _mm_div_ps(me, _mm_set1_ps(len));*/

		__m128 dp = _mm_dp_ps(me, me, 0x7f);

		// compute rsqrt of the dot product
		dp = _mm_sqrt_ps(dp);

		// vec * rsqrt(dot(vec, vec))
		me = _mm_div_ps(me, dp);
#endif
	}
	// Generate random unit vector
	CUDA_HD static inline Vector3 random() {
		return Vector3(rand() * 2.0f / RAND_MAX - 1.0f, rand() * 2.0f / RAND_MAX - 1.0f, rand() * 2.0f / RAND_MAX - 1.0f).normal();
	}
	// Get reflected vector acording to normal 
	CUDA_HD static inline Vector3 reflect(Vector3 dir, Vector3 norm) 
	{
		return dir - norm * ((dir * norm) * 2.f); 
	}
	// Try to refract vector
	CUDA_HD static inline Vector3 refract(Vector3 dir, Vector3 normal, float n1, float n2) // n1 - was, n2 - become
	{
		/*if (abs(n1 - 1.f) > EPS) {
			return refract(dir, normal * -1, n2, n1);
		}*/
		float eta = n1 / n2;
		float cosa = -(dir * normal);
		float sina = sqrtf(1.f - cosa * cosa);
		float sinb = eta * sina;
		float cosb = sqrtf(1.f - sinb * sinb);
		return (0 <= sinb && sinb <= 1.0f) ? (dir + normal * cosa) * eta - normal * cosb : Vector3(2, 0, 0);
	}
	CUDA_HD inline int colorToRGB() const { return (int(255.9f * e[0]) << 16) + (int(255.9f * e[1]) << 8) + int(255.9f * e[2]); }
	CUDA_HD inline int colorToBGR() const { return (int(255.9f * e[2]) << 16) + (int(255.9f * e[1]) << 8) + int(255.9f * e[0]); }
	CUDA_HD inline string colorName() const { return std::to_string(colorToRGB()); }
};
#endif