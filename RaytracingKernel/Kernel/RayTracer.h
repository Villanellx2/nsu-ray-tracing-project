#ifndef RAYTRACER
#define RAYTRACER
/* Main headers:
*	Renderer.h - rendering sturcture
*	MapManager.h - load images
*/
#include "./Renderer.h" 
#include "./Geometry/Material/MapManager.h"

/* Primitives:
*	Plane
*	Sphere
*	Cone
*	Cylinder
*/
#include "./Geometry/Primitives/Plane.h"
#include "./Geometry/Primitives/Cylinder.h"
#include "./Geometry/Primitives/Cone.h"
#include "./Geometry/Primitives/Sphere.h"
#include "./Geometry/Primitives/Box.h"

/* Light objects
*	Point light
*	Global light 
*	Plane light (source of light of plane shape)
*/

#include "./Light/PointLight.h"
#include "./Light/GlobalLight.h"
#include "./Light/PlaneLight.h"

#endif