#ifndef OBJECT
#define  OBJECT
#include "./Kernel.h"
#include "./Vector3.h"
#include "./Quaternion.h"
#include "./BBox.h"

struct HitResult
{
	//float col;
	Vector3 nPos;
	Vector3 nNormal;
	float dist = 1e9;
	float maxDist = 1e4;
	uint16_t id;
	float u;
	float v;
};


class Object
{
public:
	Vector3 location;
	Quaternion rotation;
	Vector3 scale;
	//normalized local axis
	Vector3 Xn = Vector3(1, 0, 0);
	Vector3 Yn = Vector3(0, 1, 0);
	Vector3 Zn = Vector3(0, 0, 1);
	//scale-applied local axis
	Vector3 X = Vector3(1, 0, 0);
	Vector3 Y = Vector3(0, 1, 0);
	Vector3 Z = Vector3(0, 0, 1);
	//normalized complementar axis: (a * X + b * Y + c * Z) = ((a, b, c) * Xc, (a, b, c) * Yc, (a, b, c) * Zc)
	Vector3 Xcn = Vector3(1, 0, 0);
	Vector3 Ycn = Vector3(0, 1, 0);
	Vector3 Zcn = Vector3(0, 0, 1);
	//complementar scale-applied axis:
	Vector3 Xc = Vector3(1, 0, 0);
	Vector3 Yc = Vector3(0, 1, 0);
	Vector3 Zc = Vector3(0, 0, 1);
	

	// 0 - lower point, 1 - upper point
	BBox bb;

	bool bAlive = 1;

	CUDA_HD Object(Vector3 loc = Vector3(), Quaternion rot = Quaternion(), Vector3 scl = Vector3(1, 1, 1))
	{
		location = loc;
		scale = scl;
		if (rot.isUnit())
			this->rotate(rot);
		updateBB();
		bAlive = 1;
		Xcn = Vector3(Xn.e[0], Yn.e[0], Zn.e[0]);
		Ycn = Vector3(Xn.e[1], Yn.e[1], Zn.e[1]);
		Zcn = Vector3(Xn.e[2], Yn.e[2], Zn.e[2]);
		X = Xn / scale.e[0];
		Y = Yn / scale.e[1];
		Z = Zn / scale.e[2];
		Xc = Vector3(Xn.e[0] * scale.e[0], Yn.e[0] * scale.e[1], Zn.e[0] * scale.e[2]);
		Yc = Vector3(Xn.e[1] * scale.e[0], Yn.e[1] * scale.e[1], Zn.e[1] * scale.e[2]);
		Zc = Vector3(Xn.e[2] * scale.e[0], Yn.e[2] * scale.e[1], Zn.e[2] * scale.e[2]);
	}

	CUDA_HD inline void remove()
	{
		bAlive = 0;
	}

	CUDA_HD inline Vector3 getLocation() const { return location; }
	CUDA_HD inline Quaternion getRotation() const { return rotation; }
	CUDA_HD inline Vector3 getScale() const { return scale; }

	CUDA_HD virtual void updateBB() {};

	CUDA_HD inline void addOffset(Vector3 Offset) { location += Offset; updateBB(); }
	CUDA_HD inline void addScale(Vector3 delta) { scale += delta; updateBB(); }
	CUDA_HD inline void rotate(Quaternion quat)
	{
		quat.rotate(Xn);
		quat.rotate(Yn);
		quat.rotate(Zn);
		Xcn = Vector3(Xn.e[0], Yn.e[0], Zn.e[0]);
		Ycn = Vector3(Xn.e[1], Yn.e[1], Zn.e[1]);
		Zcn = Vector3(Xn.e[2], Yn.e[2], Zn.e[2]);
		X = Xn / scale.e[0];
		Y = Yn / scale.e[1];
		Z = Zn / scale.e[2];
		Xc = Vector3(Xn.e[0] * scale.e[0], Yn.e[0] * scale.e[1], Zn.e[0] * scale.e[2]);
		Yc = Vector3(Xn.e[1] * scale.e[0], Yn.e[1] * scale.e[1], Zn.e[1] * scale.e[2]);
		Zc = Vector3(Xn.e[2] * scale.e[0], Yn.e[2] * scale.e[1], Zn.e[2] * scale.e[2]);
		updateBB();
	}

	CUDA_HD inline void setLocation(Vector3 newLocation)
	{
		location = newLocation;
		updateBB();
	}

	CUDA_HD inline void setScale(Vector3 newScale)
	{
		X /= newScale.e[0] / scale.e[0];
		Y /= newScale.e[1] / scale.e[1];
		Z /= newScale.e[2] / scale.e[2];
		Xc = Vector3(Xn.e[0] * newScale.e[0], Yn.e[0] * newScale.e[1], Zn.e[0] * newScale.e[2]);
		Yc = Vector3(Xn.e[1] * newScale.e[0], Yn.e[1] * newScale.e[1], Zn.e[1] * newScale.e[2]);
		Zc = Vector3(Xn.e[2] * newScale.e[0], Yn.e[2] * newScale.e[1], Zn.e[2] * newScale.e[2]);
		scale = newScale;
		updateBB();
	}

	CUDA_HD inline void toLocalCoord(Vector3& Point) const
	{
		Point = Vector3(Point * X, Point * Y, Point * Z);
	}

	CUDA_HD inline void toGlobalCoord(Vector3& Point) const
	{
		Point = Vector3(Point * Xc, Point * Yc, Point * Zc);
	}

	CUDA_HD inline void localizeDirection(Vector3& dir) const
	{
		dir = Vector3(dir * X, dir * Y, dir * Z);
		dir.normalize();
	}
	CUDA_HD inline void globalizeDirection(Vector3& dir) const
	{
		dir = Vector3(dir * Xcn, dir * Ycn, dir * Zcn);
		dir.normalize();
	}
	CUDA_HD inline Ray localize(const Ray& r) const
	{
		Vector3 nOrigin = r.start - location;
		Vector3 nDir = r.dir;
	
		toLocalCoord(nOrigin);
		localizeDirection(nDir);
		
		return Ray::makeRay(nOrigin, nDir);
	}

	CUDA_HD inline void globalize(HitResult& hit)
	{
		toGlobalCoord(hit.nPos);
		hit.nNormal /= scale;
		globalizeDirection(hit.nNormal);
		hit.nPos += location;
	}

	CUDA_HD inline void* operator new(size_t size) {
#ifdef __CUDA_ARCH__
		return malloc(size);
#else
		return _aligned_malloc(size, 16);
#endif
	}

	CUDA_HD inline void operator delete(void * p) {
#ifdef __CUDA_ARCH__
		free(p);
#else
		_aligned_free(p);
#endif
	}
};
#endif