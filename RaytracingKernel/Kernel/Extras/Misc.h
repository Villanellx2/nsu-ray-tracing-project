#ifndef RMATH_h
#define RMATH_h
#include "./../Kernel.h"
#include "./../Vector3.h"
#include "emmintrin.h"
#define min(a, b) ((a) < (b)? (a) : (b))
#define max(a, b) ((a) > (b)? (a) : (b))

class RandomGenerator
{
	uint32_t state;
	RandomGenerator()
	{
		state = (int32_t)time(NULL);
	}
public:
	
	inline static RandomGenerator& getInstance()
	{
		static RandomGenerator gen;
		return gen;
	}

	
	inline void setSeed(uint32_t seed)
	{
		this->state = seed;
	}

	inline uint32_t rand32()
	{
		uint32_t x = state;
		x ^= x << 13;
		x ^= x >> 17;
		x ^= x << 5;
		return state = x;
	}

	inline float randf()
	{
		union
		{
			float f;
			uint32_t i;
		};
		i = 0x3F800000U | (rand32() >> 9);
		return f - 1.0f;
	}
	inline Vector3 randV2(Vector3 min, Vector3 max)
	{
		return (Vector3(_mm_set_ps(0, randf(), randf(), 0)) | (max - min)) + min;
	}
	inline Vector3 randV3(Vector3 min, Vector3 max) 
	{
		return (Vector3(_mm_set_ps(0, randf(), randf(), randf())) | (max - min)) + min;
	}
};

inline Vector3 clamp(Vector3 a, Vector3 min, Vector3 max)
{
	return Vector3(_mm_max_ps(min.me, _mm_min_ps(a.me, max.me)));
}

inline Vector3 minVector(Vector3 a, Vector3 b)
{
#ifdef __CUDA_ARCH__
	return Vector3(min(a.x(), b.x()), min(a.y(), b.y()), min(a.z(), b.z()));
#else
	return Vector3(_mm_min_ps(a.me, b.me));
#endif
}

inline Vector3 maxVector(Vector3 a, Vector3 b)
{
#ifdef __CUDA_ARCH__
	return Vector3(max(a.x(), b.x()), max(a.y(), b.y()), max(a.z(), b.z()));
#else
	return Vector3(_mm_max_ps(a.me, b.me));
#endif
}

bool getFormat(std::string& str, std::string& format);

#endif