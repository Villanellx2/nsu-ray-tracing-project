#include "./Misc.h"

bool getFormat(string& str, string& format)
{
	size_t last = str.size() - 1;
	for (int i = 0; i < last; i++)
	{
		if (str[last - i] == '.')
		{
			reverse(format.begin(), format.end());
			return 1;
		}
		format.push_back(str[last - i]);
	}
	return 0;
}