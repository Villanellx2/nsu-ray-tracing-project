#include "./KDTree.h"

CUDA_H int Scene::addElem(BasicShape* bSp)
{
	if (cnt < MAX_OBJECTS)
	{
		cnt++;
		
		listNode *tmp = lfree->start->next;
		sp[tmp->nodeId] = bSp;

		bSp->setListNode(tmp);
		
		tmp->removeElem();

		ltaken->addElem(tmp);
		return tmp->nodeId;
	}
	return -1;
}

CUDA_H void Scene::removeElem(int id)
{
	sp[id]->kill();
	listNode* cur = (listNode*)sp[id]->GetListNode();
	sp[id]->setListNode(NULL);
	
}

CUDA_H void Scene::initScene(std::vector<BasicShape*> bsv)
{
	cnt = bsv.size();
	for (size_t i = 0; i < bsv.size(); i++)
	{
		sp[i] = bsv[i];
		listNode* node = (listNode*)malloc(sizeof(listNode));
		
		ltaken->addElem(node);
		node->nodeId = i;
		bsv[i]->setListNode(node);
	}
	for (size_t i = bsv.size(); i < MAX_OBJECTS; i++)
	{
		listNode* node = (listNode*)malloc(sizeof(listNode));
		lfree->addElem(node);
		node->nodeId = i;
	}
}

CUDA_H  void Scene::rebuild()
{
	nodesCount = 1;
	nodes[0].ol = (uint16_t*)malloc(cnt * sizeof(uint16_t));
	listNode *cur = ltaken->start->next;
	for (int i = 0; cur->nodeId != -1; i++, cur = cur->next)
	{
		nodes[0].ol[i] = cur->nodeId;
	}
	for (int i = 0; i < nodesCount; i++)
	{
		split(i);
	}
}


CUDA_H void Scene::split(int id)
{
	if (nodes[id].depth == MAX_DEPTH)
	{
		return;
	}
	Vector3 nodeDeltas = nodes[id].bb[1] - nodes[id].bb[0];

	float curSAH = INTERSECT_COST * nodes[id].olsize;

	//printf("SAH: %f\n", curSAH);

	float SA = nodeDeltas.x() * nodeDeltas.y() + nodeDeltas.y() * nodeDeltas.z() + nodeDeltas.x() * nodeDeltas.z();

	float ansSAH = curSAH;


	Vector3 dv = nodeDeltas / TREE_DENSITY;

	double nodeVolume = nodeDeltas.x() * nodeDeltas.y() * nodeDeltas.z();

	int ps[3][TREE_DENSITY];
	int ss[3][TREE_DENSITY];

	int pps[3][TREE_DENSITY];
	int sss[3][TREE_DENSITY];

	for (int i = 0; i < nodes[id].olsize; i++)
	{
		Vector3 ddv = sp[nodes[id].ol[i]]->BoundingBox[0] - nodes[id].bb[0];
		Vector3 ids = ddv / dv;
		sss[0][int(ids.getByID(0) + EPS)]++;
		sss[1][int(ids.getByID(1) + EPS)]++;
		sss[2][int(ids.getByID(2) + EPS)]++;

		ddv = nodes[id].bb[1] - sp[nodes[id].ol[i]]->BoundingBox[1];
		ids = ddv / dv;

		pps[0][nodes[id].olsize - 1 - int(ids.getByID(0) + EPS)]++;
		pps[0][nodes[id].olsize - 1 - int(ids.getByID(1) + EPS)]++;
		pps[0][nodes[id].olsize - 1 - int(ids.getByID(2) + EPS)]++;
	}

	int pss[3] = { 0,0,0 };
	int sps[3] = { 0,0,0 };

	std::pair<int, int> minval;
	for (int i = 0; i < TREE_DENSITY; i++)
	{
		pss[0] += pps[0][i];
		pss[1] += pps[1][i];
		pss[2] += pps[2][i];

		sps[0] += sss[0][i];
		sps[1] += sss[1][i];
		sps[2] += sss[2][i];

		ps[0][i] = pss[0];
		ps[1][i] = pss[1];
		ps[2][i] = pss[2];

		ss[0][i] = sps[0];
		ss[1][i] = sps[1];
		ss[2][i] = sps[2];
	}


}

CUDA_H void Scene::acceptPacket(Package* package, bool fastUpdate)
{
	int id;
	if (id = package->isDeletion() != -1)
	{
		sp[id]->kill();
	}
	else if (package->isCreation())
	{
		id = addElem(package->shape);
	}
	else
	{
		id = package->getid();
		sp[id]->AddOffset(package->getLocation());
		sp[id]->Rotate(package->getRotation());
		sp[id]->AddScale(package->getScale());
	}
	if (!fastUpdate)
		rebuild();
	else
		badid = id;
}

CUDA_H void Scene::getSceneObjects(std::vector<BasicShape*>& objs)
{
	objs.clear();
	listNode *a = ltaken->start->next;
	while (a->nodeId != -1)
	{
		objs.push_back(sp[a->nodeId]);
		a = a->next;
	}
}