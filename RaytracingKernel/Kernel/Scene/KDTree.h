#ifndef KDTREE1
#define KDTREE1
#include "./Scene.h"

#define COUNT nodesCount

//#define TDEBUG

struct SceneNode
{
	Vector3 bb[2];
	int olsize;
	uint16_t* ol;
	uint16_t links[6];
	uint16_t left, right, par;
	char depth;
	char leaf;
	CUDA_HD SceneNode()
	{
		links[0] = links[1] = links[2] = links[3] = links[4] = links[5] = -1;
		left = right = par = -1;
		ol = NULL;
		olsize = 0;
		depth = 0;
		leaf = 1;
	}
};

struct TraceInfo
{
	short nodeId;
	unsigned long long inter[1024];
	CUDA_HD TraceInfo(short Start = 0)
	{
		nodeId = Start;
		memset(inter, 0, sizeof(long long) * 1024);
	}
};

class KDTree : public Scene
{
private:
	SceneNode nodes[(1 << (MAX_DEPTH + 1))];
	
public:

	CUDA_HD KDTree()
	{
		nodesCount = 0;
		
		lfree = new List();
		ltaken = new List();

		cnt = 0;
		badid = -1;
	}

	CUDA_HD ~KDTree()
	{
		delete lfree;
		delete ltaken;
	}
	/*{
		if (nSp->id == 1)
		{
			cudaMallocManaged(&sp[cnt], sizeof(Plane));
			memcpy(sp[cnt], nSp, sizeof(Plane));
		}
		if (nSp->id == 2)
		{
			cudaMallocManaged(&sp[cnt], sizeof(Sphere));
			memcpy(sp[cnt], nSp, sizeof(Sphere));
		}
		if (nSp->id == 3)
		{
			cudaMallocManaged(&sp[cnt], sizeof(Cylinder));
			memcpy(sp[cnt], nSp, sizeof(Cylinder));
		}
		cnt++;
	}*/
	/*
	CUDA_H void resize(int nSize)
	{
		if (nSize > spsize)
		{
			BasicShape** sp1;
			cudaMallocManaged(&sp1, nSize * sizeof(BasicShape*));
			if (spsize)
			{
				memcpy(sp1, sp, spsize * sizeof(BasicShape*));
				cudaFree(sp);
			}
			sp = sp1;
			spsize = nSize;
		}
	}

	CUDA_H void resizeLS(int nSize)
	{
		cudaMallocManaged((void**)&d_ls, sizeof(LightSource*) * nSize);
		lssize = nSize;
	}

	CUDA_H  void addLS(LightSource* nls)
	{
		d_ls[cntLS++] = nls;
	}
	*/
	CUDA_H void initScene(std::vector<BasicShape*> bsv);

	CUDA_H  void rebuild();
	/*
	{
		nodesCount = 1;
		cudaMallocManaged((void**)&nodes[0].ol, sizeof(short) * cnt);
		nodes[0].olsize = cnt;
		for (short i = 0; i < cnt; i++)
		{
			nodes[0].ol[i] = i;
		}
		//printf("cnt: %d\n", cnt);
		for (int i = 0; i < 6; i++)
		{
			nodes[0].links[i] = -1;
		}
		nodes[0].par = nodes[0].left = nodes[0].right = -1;
		nodes[0].low = Vector3(-1e3, -1e3, -1e3);
		nodes[0].up = Vector3(1e3, 1e3, 1e3);
		nodes[0].leaf = 1;
		for (int i = 0; i < COUNT; i++)
		{
			split(i);
		}
	}
	*/
	CUDA_H  size_t getSize();
	/*
	{
		return size_t(sizeof(Scene) + sizeof(BasicShape*) * spsize);
	}
	*/
	CUDA_HD int val()
	{
		return nodes[3].links[4];
	}

	CUDA_HD bool PointInNode(int id, Vector3 Point);
	/*
	{
		//printf("Is in node?\n");
		for (int i = 0; i < 3; i++)
		{
			//printf("Check axis: %d; %f %f %f\n", i, nodes[id].low.e[i], Point.e[i], nodes[id].up.e[i]);
			if (nodes[id].low.e[i] <= Point.e[i] && Point.e[i] < nodes[id].up.e[i])
			{
				continue;
			}
			else
			{
				return 0;
			}
		}
		return 1;
	}
	*/
	CUDA_HD float CalcDirectionalLight(Ray r, int col, int start);
	/*
	{
		float val = 0;
		for (int i = 0; i < cntLS; i++)
		{
			float dist = (d_ls[i]->GetLocation() - r.Start).magnitude();
			HitResult res;
			res.maxDist = dist;
			Ray r1 = Ray(r.Start, (d_ls[i]->GetLocation() - r.Start).normal());
			if (RayCast(r1, col, res, start) == -1 || res.dist > (d_ls[i]->GetLocation() - r.Start).magnitude())
			{
				val += d_ls[i]->GetIntencity(r, col);
			}
		}
		return val;
	}
	*/
	CUDA_HD int Traverse(int id, Ray r);
	/*
	{
		while (!nodes[id].leaf)
		{
			char f = 1;
			short left = nodes[id].left;
			short right = nodes[id].right;
			//printf("\ncurrent ray: (%f, %f, %f), (%f, %f, %f)\n", r.Start.e[0], r.Start.e[1], r.Start.e[2], r.Dir.e[0], r.Dir.e[1], r.Dir.e[2]);
			//printf("left box: (%f, %f, %f), (%f, %f, %f)\n", nodes[left].low.e[0], nodes[left].low.e[1], nodes[left].low.e[2], nodes[left].low.e[0], nodes[left].up.e[1], nodes[left].up.e[2]);
			//printf("right box: (%f, %f, %f), (%f, %f, %f)\n", nodes[right].low.e[0], nodes[right].low.e[1], nodes[right].low.e[2], nodes[right].low.e[0], nodes[right].up.e[1], nodes[right].up.e[2]);

			for (int i = 0; i < 3; i++)
			{
				if (r.Start.e[i] > nodes[left].up.e[i])
				{
					f = 0;
				//	printf("Wat? %f %f\n", r.Start.e[i], nodes[left].up.e[i]);
				}
				else
				{
					//printf("Seems ok? %f %f\n", r.Start.e[i], nodes[left].up.e[i]);
				}
			}
			
			if (f)
			{
				id = left;
			}
			else
			{
				id = nodes[id].right;
			}
		}
#ifdef TDEBUG
		printf("Now in node %d\n", id);
		printf("(%f, %f, %f), (%f, %f, %f)\n", nodes[id].low.e[0], nodes[id].low.e[1], nodes[id].low.e[2], nodes[id].up.e[0], nodes[id].up.e[1], nodes[id].up.e[2]);
#endif
		return id;
	}
	*/
	CUDA_HD bool FindIntersection(const Ray& r, TraceInfo& info, int id, HitResult& res, int colId);
	/*
	{
		float dist = 1e9;
		bool found = 0;
		//printf("current ray: (%f, %f, %f), (%f, %f, %f)\n", r.Start.e[0], r.Start.e[1], r.Start.e[2], r.Dir.e[0], r.Dir.e[1], r.Dir.e[2]);
		//printf("current box: (%f, %f, %f), (%f, %f, %f)\n", nodes[id].low.e[0], nodes[id].low.e[1], nodes[id].low.e[2], nodes[id].up.e[0], nodes[id].up.e[1], nodes[id].up.e[2]);
		//printf("nodes count: %d\n", nodes[id].olsize);
		for (int i = 0; i < nodes[id].olsize; i++)
		{
#ifdef TDEBUG
			printf("Intersecting id: %d\n", nodes[id].ol[i]);
#endif
			HitResult Curres;
			int id1 = nodes[id].ol[i];
			//printf("check hit with %d ", id1);
			if (d_sp[nodes[id].ol[i]]->hit(r, Curres, colId))
			{
#ifdef TDEBUG
				printf("Intersected: %d\n", id1);
				printf("Position: (%f, %f, %f)\n", Curres.nPos.e[0], Curres.nPos.e[1], Curres.nPos.e[2]);
#endif
				//printf("Got hit: (%f, %f, %f)\n", Curres.nPos.e[0], Curres.nPos.e[1], Curres.nPos.e[2]);
				if (PointInNode(id, Curres.nPos) && Curres.dist <= Curres.maxDist)
				{
					//printf("Works?\n");
					Curres.id = nodes[id].ol[i];
					if (dist > Curres.dist)
					{
						//printf("switch\n");
						dist = Curres.dist;
						res = Curres;
					}
					found = 1;
					//printf("FOUND!\n");
				}
			}
			//printf("Intersected list: %x\n", info.inter[0]);
		}
		//printf("\n");
		return found;
	}
	*/
	CUDA_HD int Move(const Ray& r, int id, float& accdist);
	/*
	{
		float dist = 1e9;
		int nid = 0;
#ifdef TDEBUG
		printf("Current location: (%f, %f, %f)\n", r.Start.e[0], r.Start.e[1], r.Start.e[2]);
#endif
		for (int i = 0; i < 3; i++)
		{
			if (r.Dir.e[i] == 0)
			{
				continue;
			}
			float mx = (nodes[id].up.e[i] + 1 - r.Start.e[i]) / r.Dir.e[i];
			float nx = (nodes[id].low.e[i] - 1 - r.Start.e[i]) / r.Dir.e[i];
			//printf("Axis move: %d, dist: %f, %f\n", i, nx, mx);

			float md = (nodes[id].up.e[i] - r.Start.e[i]) / r.Dir.e[i];
			float nd = (nodes[id].low.e[i] - r.Start.e[i]) / r.Dir.e[i];
			//printf("Axis move1: %d, dist: %f, %f\n", i, nd, md);

			if (mx >= 0)
			{
				if (dist > md)
				{
					dist = md;
					nid = i * 2;
				}
			}
			if (nx >= 0)
			{
				if (dist > nd)
				{
					dist = nd;
					nid = i * 2 + 1;
				}
			}
		}
		//printf("Dist: %f, selected: %d\n", dist, nid);
		accdist = dist;
		//printf("Move in node %d\n", nid);
		//printf("(%f, %f, %f), (%f, %f, %f)\n", nodes[nodes[id].links[nid]].low.e[0], nodes[nodes[id].links[nid]].low.e[1], nodes[nodes[id].links[nid]].low.e[2], nodes[nodes[id].links[nid]].up.e[0], nodes[nodes[id].links[nid]].up.e[1], nodes[nodes[id].links[nid]].up.e[2]);
		return nodes[id].links[nid];
	}
	*/
	CUDA_HD int RayCast(Ray r, int colId, HitResult& res, int start);
	/*
	{
		int cur = start;
		float accumulate = 0;
		bool hit = 0;
		TraceInfo info;

		//int c = 0;
		while (1)
		{
			cur = Traverse(cur, r);
			//if (!c) cnt = 0;
			//c = 1;
			if (FindIntersection(r, info, cur, res, colId))
			{
				hit = 1;
				res.dist += accumulate;
				//printf("Returning\n");
				return cur;
			}
			else
			{
				float dist;
				cur = Move(r, cur, dist);
				r.Start = r.Start + r.Dir * dist; 

#ifdef TDEBUG
				printf("Current location: (%f, %f, %f)\n", r.Start.e[0], r.Start.e[1], r.Start.e[2]);
#endif
				res.maxDist -= dist;
				accumulate += dist;
#ifdef TDEBUG
				printf("Accumulations: %f\n", accumulate);
#endif
				if (cur == -1) return -1;
			}
		}

	}
	*/
	CUDA_HD float TraceRay(Ray initial, int colId, int start, int maxDepth, int i1, int j);
	/*
	{
		float val = 0;
		float attenuation = 1;
		Ray r = initial;
		for (int i = 0; i < maxDepth; i++)
		{
			HitResult res;
			start = RayCast(r, colId, res, start);
			//printf("%d\n", start);
			if (start < 0)
			{
				//if (r.Dir.e[2] < -0.05)
				//{
				//	printf("Ogo vau!!! %d %d\n", i1, j);
				//}
				float t = 0.5f * (r.Dir.y() + 1.0f);
				val += attenuation * (Vector3(1.0, 1.0, 1.0) * (1.0f - t) + Vector3(0.5, 0.7, 1.0) * t).e[colId];
				break;
			}
			//if (i1 == 0)
			//{
			//	printf("%d %f %f\n", j, res.nPos.e[0], res.nPos.e[2]);
			//}

			//float DirLight = min(1, max(0.05, CalcDirectionalLight(Ray(res.nPos + res.nNormal * 0.01, res.nNormal), colId, start)));
			//printf("Attenuation: %f\n", attenuation);
			//printf("Value: %f\n", res.col);
			val += attenuation * d_sp[res.id]->Mat.Coefs.e[0] * res.col; //* DirLight;
			attenuation *= d_sp[res.id]->Mat.Coefs.e[1];
			r.Dir = r.Dir - res.nNormal * (res.nNormal * r.Dir * 2);
			r.Start = res.nPos;
			if (attenuation < 0.05) break;
		}
		//printf("Value: %f\n", val);
		return min(1.0, val);
	}
	*/
	CUDA_H void split(int id);
	/*
	{
		if (nodes[id].depth == 10)
		{
			return;
		}
		float dd[3] = {};

		float dx = nodes[id].up.x() - nodes[id].low.x();
		float dy = nodes[id].up.y() - nodes[id].low.y();
		float dz = nodes[id].up.z() - nodes[id].low.z();

		float curSAH = INTERSECT_COST * nodes[id].olsize;
		
		//printf("SAH: %f\n", curSAH);

		float SA = dx * dy + dy * dz + dx * dz;

		float ansSAH = curSAH;
		int gcut = -1;
		int axid = -1;
		float gddv;


		short gss[DENSITY];
		short gps[DENSITY];

		
		for (int axis = 0; axis < 3; axis++)
		{
			//printf("a: %f, b: %f\n", sp[0]->BoundingLower.e[axis], sp[1]->BoundingLower.e[axis]);
			std::sort(nodes[id].ol, nodes[id].ol + nodes[id].olsize,
				[&axis, this](short& a, short& b) -> bool
			{
				return sp[a]->BoundingLower.e[axis] < sp[b]->BoundingLower.e[axis];
			});

			
			float dv = nodes[id].up.e[axis] - nodes[id].low.e[axis];

			float ddv = dv / DENSITY;

			float cv = nodes[id].low.e[axis];

			int p = 0;
			short ps[DENSITY];
			short ss[DENSITY];

			//printf("Prefix function: %d\n", axis);

			for (int i = 0; i < DENSITY; i++)
			{
				while (p < nodes[id].olsize && sp[nodes[id].ol[p]]->BoundingLower.e[axis] < cv) p++;
				ps[i] = p;
				cv += ddv;
				//printf("i: %d, dist: %f, ps: %d\n", i, cv, ps[i]);
			}
			std::sort(nodes[id].ol, nodes[id].ol + nodes[id].olsize,
				[&axis, this](short& a, short& b) -> bool
			{
				return sp[a]->BoundingUpper.e[axis] > sp[b]->BoundingUpper.e[axis];
			});

			p = 0;
			cv -= ddv;
			for (int i = DENSITY - 1; i >= 0; i--)
			{
				while (p < nodes[id].olsize && sp[nodes[id].ol[p]]->BoundingUpper.e[axis] > cv) p++;
				ss[i] = p;
				cv -= ddv;
			}

			
			float mul = dx * dy * dz / dv;
			float sum = dx + dy + dz - dv;
			float mSAH = curSAH;
			int cut = -1;

			for (int i = 1; i < DENSITY - 1; i++)
			{
				float crvSAH = ps[i] * ((mul + sum * i * ddv)/SA) + ss[i] * ((mul + sum * (DENSITY - i) * ddv) / SA);
				if (mSAH > crvSAH)
				{
					mSAH = crvSAH;
					cut = i;
				}
			}
			mSAH *= INTERSECT_COST;
			mSAH += DEFAULT_COST;

			if (mSAH <= 0)
			{
				printf("Mistake in node: %d\n", id);
				exit(-1);
			}
			if (mSAH < ansSAH)
			{
				axid = axis;
				gcut = cut;
				ansSAH = mSAH;
				gddv = ddv;
			//	printf("Updating min function: %d %d\n", axis, cut);
				for (int i = 0; i < DENSITY; i++)
				{
					gps[i] = ps[i];
					gss[i] = ss[i];
				}
			}
		}



		if (ansSAH < curSAH)
		{
			//printf("division axis: %d, on cut: %d, size of subtrees: %d, %d\n", axid, gcut, gps[gcut], gss[gcut]);
			//printf("id: %d, ps: %d, ss: %d\n", id, gps[gcut], gss[gcut]);
			if (gps[gcut] + gss[gcut] < nodes[id].olsize)
			{
				printf("Error in node: %d\n", id);
				exit(-1);
			}
			nodes[id].leaf = 0;
			nodes[id].left = nodesCount;
			nodes[id].right = nodesCount + 1;

			nodes[nodesCount].leaf = nodes[nodesCount + 1].leaf = 1;

			for (int i = 0; i < 6; i++)
			{
				nodes[nodesCount].links[i] = nodes[nodesCount + 1].links[i] = nodes[id].links[i];
			}

			//printf("%d %d\n", axid, gcut);

			nodes[nodesCount].par = nodes[nodesCount + 1].par = id;
			nodes[nodesCount].links[axid * 2] = nodesCount + 1;
			nodes[nodesCount + 1].links[axid * 2 + 1] = nodesCount;

			nodes[nodesCount].low = nodes[nodesCount + 1].low = nodes[id].low;
			nodes[nodesCount + 1].low.e[axid] += gcut * gddv;
			nodes[nodesCount].up = nodes[nodesCount + 1].up = nodes[id].up;
			nodes[nodesCount].up.e[axid] -= (DENSITY - gcut) * gddv;

			nodes[nodesCount].olsize = gps[gcut];
			nodes[nodesCount + 1].olsize = gss[gcut];
			
			if (gps[gcut])
			{	
				std::sort(nodes[id].ol, nodes[id].ol + nodes[id].olsize,
					[&axid, this](short& a, short& b) -> bool
				{
					return sp[a]->BoundingLower.e[axid] < sp[b]->BoundingLower.e[axid];
				});

				//std::cout << "ids: ";
				/*for (int i = 0; i < nodes[id].olsize; i++)
				{
					std::cout << nodes[id].ol[i] << " ";
				}
				std::cout << std::endl << ", move left " << gps[gcut] << "elems" << std::endl;
				cudaMallocManaged(&nodes[nodesCount].ol, gps[gcut] * sizeof(short));
				for (int i = 0; i < gps[gcut]; i++)
				{
					if (sp[nodes[id].ol[i]]->maxNode == id)
					{
						sp[nodes[id].ol[i]]->maxNode = nodesCount;
					}
					nodes[nodesCount].ol[i] = nodes[id].ol[i];
				}
			}
			if (gss[gcut])
			{
				std::sort(nodes[id].ol, nodes[id].ol + nodes[id].olsize,
					[&axid, this](short& a, short& b) -> bool
				{
					return sp[a]->BoundingUpper.e[axid] > sp[b]->BoundingUpper.e[axid];
				});
				/*std::cout << "ids: ";
				for (int i = 0; i < nodes[id].olsize; i++)
				{
					std::cout << nodes[id].ol[i] << " ";
				}
				std::cout << std::endl << ", move right " << gss[gcut] << "elems" << std::endl;
				cudaMallocManaged(&nodes[nodesCount + 1].ol, gss[gcut] * sizeof(short));
				for (int i = 0; i < gss[gcut]; i++)
				{
					if (sp[nodes[id].ol[i]]->maxNode == id)
					{
						sp[nodes[id].ol[i]]->maxNode = nodesCount + 1;
					}
					else if (sp[nodes[id].ol[i]]->maxNode == nodesCount)
					{
						sp[nodes[id].ol[i]]->maxNode = id;
					}
					nodes[nodesCount+1].ol[i] = nodes[id].ol[i];
				}
			}
			
			if (nodes[id].olsize)
				cudaFree(nodes[id].ol);
			
			//nodes[id].olsize = 0;

			
			nodes[nodesCount].depth = nodes[nodesCount + 1].depth = nodes[id].depth + 1;

			nodesCount += 2;
		}
		else
		{
			return;
		}
	}
	*/
	CUDA_H bool validate();
	/*
	{
		for (int i = 0; i < nodesCount; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				if (nodes[i].low.e[j] >= nodes[i].up.e[j])
				{
					printf("i: %d, j: %d", i, j);
					return false;
				}
			}
			if (nodes[i].leaf)
			{
				//printf("Bounding box: (%f, %f, %f), (%f, %f, %f)", nodes[i].low.e[0], nodes[i].low.e[1], nodes[i].low.e[2], nodes[i].up.e[0], nodes[i].up.e[1], nodes[i].up.e[2]);
				//printf("\nContains: ");
				for (int j = 0; j < nodes[i].olsize; i++)
				{
				//	printf("%d ", nodes[i].ol[j]);
				}
				//printf("\n===\n");
			}
		}
		return true;
	}
#ifdef CUDA
	CUDA_H void CopyOnCuda(Scene** d_scene)
	{
		cudaMalloc((void**)&(*d_scene), sizeof(Scene));
		cudaMemcpy((*d_scene), this, sizeof(Scene), cudaMemcpyHostToDevice);
		printf("%d %d\n", (*d_scene), spsize);
		BasicShape** gpuSP = new BasicShape*[spsize];
		for (int i = 0; i < spsize; i++)
		{
			if (sp[i]->id == 1)
			{
				cudaMalloc(&gpuSP[i], sizeof(Plane));
				cudaMemcpy(gpuSP[i], sp[i], sizeof(Plane), cudaMemcpyHostToDevice);
			}
			else
			{
				cudaMalloc(&gpuSP[i], sizeof(Sphere));
				cudaMemcpy(gpuSP[i], sp[i], sizeof(Sphere), cudaMemcpyHostToDevice);
			}
		}
		BasicShape** d_sp;
		cudaMalloc(&d_sp, spsize * sizeof(BasicShape*));
		cudaMemcpy(d_sp, gpuSP, spsize * sizeof(BasicShape*), cudaMemcpyHostToDevice);
		cudaMemcpy(&((*d_scene)->sp), &(d_sp), sizeof(BasicShape**), cudaMemcpyHostToDevice);

		printf("%d\n", nodesCount);

		for (int i = 0; i < nodesCount; i++)
		{
			if (nodes[i].olsize == 0) continue;
			short *d_links;
			cudaMalloc(&(d_links), sizeof(short) * nodes[i].olsize);
			printf("Goose budjetniy ");
			cudaMemcpy(d_links, nodes[i].ol, sizeof(short) * nodes[i].olsize, cudaMemcpyHostToDevice);
			printf("Rabotyga nezametnuiy <(*)\n");
			cudaMemcpy(&((*d_scene)->nodes[i].ol), &d_links, sizeof(short*), cudaMemcpyHostToDevice);
		}
	}
#endif
#ifdef CUDA
	CUDA_H void ClearOnCuda(Scene **d_scene)
	{
		for (int i = 0; i < cnt; i++)
		{
			cudaFree((*d_scene)->sp[i]);
		}
		for (int i = 0; i < nodesCount; i++)
		{
			if (nodes[i].olsize)
			{
				cudaFree((*d_scene)->nodes[i].ol);
			}
		}
		cudaFree((*d_scene));
	}
#endif
	CUDA_H ~Scene()
	{
		for (int i = 0; i < cnt; i++)
		{
			delete(sp[i]);
		}
		for (int i = 0; i < cntLS; i++)
		{
			delete(d_ls[i]);
		}
		for (int i = 0; i < nodesCount; i++)
		{
			if (nodes[i].olsize)
			{
				free(nodes[i].ol);
			}
		}
		if (spsize)
			free(sp);
	}
	*/

	CUDA_H void acceptPacket(Package* package, bool fastUpdate);
	CUDA_H void getSceneObjects(std::vector<BasicShape*>& objs);
	CUDA_H void calculatePS(int nodeId, int boundingId, int axis, int* arr, double start, double dval);
};

#endif