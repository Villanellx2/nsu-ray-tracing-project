#ifndef CRay
#define CRay
#include "./Kernel.h"
#include "./Vector3.h"

struct Ray
{
	Vector3 start;
	Vector3	dir;
	float minDistance;
	float maxDistance;

private: CUDA_HD inline Ray(Vector3 origin, Vector3 direction, float minDistance = 0, float maxDistance = 1e9) : start(origin), dir(direction), minDistance(minDistance), maxDistance(maxDistance) {};
public:
	//Creates ray and normalized 
	CUDA_HD inline static Ray makeNormalizedRay(Vector3 origin, Vector3 direction, float minDistance = 0, float maxDistance = 1e9)
	{
		return Ray(origin, direction.normal(), minDistance, maxDistance);
	};
	CUDA_HD inline static Ray makeRay(Vector3 origin, Vector3 direction, float minDistance = 0, float maxDistance = 1e9)
	{
		return Ray(origin, direction, minDistance, maxDistance);
	}
	CUDA_HD inline Vector3 origin() const { return start;  }
	CUDA_HD inline Vector3 direction() const { return dir; }
	CUDA_HD inline Vector3 pointAtTime(float t) const { return start + dir * t; }
};
#endif