#ifndef CAMERA
#define CAMERA
#include "./Object.h"

struct CameraParam
{
	char type; // 0 - perspective, 1 - no persepective, 2 - fish-eye
	float fov; // if type == 0.

	int Width, Height;

	Vector3 LeftCorner = Vector3(-1, 0, -1), Horizontal = Vector3(2, 0, 0), Vertical = Vector3(0, 0, 2);
	CUDA_HD CameraParam(char nType = 0, float nFOV = 90.0f, int nWidth = 1920, int nHeight = 1080)
	{
		type = nType;
		if (type == 0)
		{
			if (nFOV >= 180)
			{
				nFOV = 179;
			}
			if (nFOV < 45)
			{
				nFOV = 45;
			}
			float v = 2.0f / tanf(nFOV / 2.0f * acosf(-1) / 180.0f);
			LeftCorner.e[1] = v;
			Vertical = Horizontal * Height / Width;
		}
		fov = nFOV;
		Height = nHeight;
		Width = nWidth;
	}
};

class Camera : public Object
{
public:
	CameraParam param;
	CUDA_HD Camera(CameraParam nParam = CameraParam(), Vector3 Loc = Vector3(), Quaternion Rot = Quaternion(), Vector3 Sc = Vector3(1, 1, 1)) : Object(Loc, Rot, Sc)
	{
		param = nParam;
	}
	CUDA_HD inline void dirAtPoint(Vector3& Start, Vector3& Direction, float u, float v)
	{
		if (param.type == 0)
		{
			Start = getLocation();
			param.Horizontal = X * 2;
			param.Vertical = Z * 2 * param.Height / param.Width;
			param.LeftCorner = -X - Z + Y;
			Direction = (param.LeftCorner + param.Horizontal * v + param.Vertical * u).normal();
			globalizeDirection(Direction);
		}
		else if (param.type == 1)
		{
			Start = getLocation() + Z * (u - 0.5f) * 2.f  + X * (v - 0.5f) * 2.f;
			Direction = Y;
		}
		else if (param.type == 2)
		{
			Start = getLocation();
			float  alpha = param.fov * (v - 0.5f) * acosf(-1) / 180.0f;
			Direction = Vector3(3.0f * sinf(alpha), 3.0f * cosf(alpha), param.LeftCorner.z() + param.Vertical.z() * u).normal();
			globalizeDirection(Direction);
		}
		else if (param.type == 3)
		{
			Start = getLocation();
			float alpha = param.fov * (v - 0.5f) * acosf(-1) / 180.0f;
			float beta = (u - 0.5f) * acosf(-1) / 3.0f;
			Direction = Vector3(sinf(alpha) * cosf(beta), cosf(alpha) * cosf(beta), sinf(beta));
			Direction = Vector3(Xcn * Direction, Ycn * Direction, Zcn * Direction);
			globalizeDirection(Direction);
		}
		else if (param.type == 4)
		{
			Start = getLocation();
			float alpha = param.fov * (v - 0.5f) * acosf(-1) / 180.0f;
			float beta = (u - 0.5f) * acosf(-1) / 3.0f;
			Vector3 c = Vector3(sinf(alpha), cosf(alpha), sinf(beta));
			Direction = Vector3(Xcn * c, Ycn * c, Zcn * c).normal()/* (1 + cosf(acosf(-1) * 1.5f * (v - 0.5)))*/;
			globalizeDirection(Direction);
		}
		else
		{
			Start = getLocation();
			float phi = param.fov * acosf(-1) / 360;
			Vector3 v1 = Start - X * sinf(phi) + Y * cosf(phi);
			
			Vector3 v2 = v1 + X * 2 * sinf(phi);

			float alpha = atanf(sinf(phi) * (v - 0.5f) / (cosf(phi) + 1));
			
			Direction = Vector3(3.0f * sinf(alpha * 2), 3.0f * cosf(alpha * 2), param.LeftCorner.e[2] + param.Vertical.e[2] * u).normal();
			globalizeDirection(Direction);
		}
	}
};

#endif