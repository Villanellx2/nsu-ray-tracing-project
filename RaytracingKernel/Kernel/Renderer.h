#ifndef RENDERER_H
#define RENDERER_H

#include "./Kernel.h"
#include "./Camera.h"
#include "./Scene/Scene.h"
#include "./Scene/BVHTree.h"
#include "./Light/LightManager.h"

enum RenderType
{
	UNLIT,
	LIT
};

class Renderer
{
private:

	uint8_t **image;
	int32_t **ids;
	int32_t samplingRate = 1;
	RenderType type;
	TextureMap* skyMap;
#ifdef CUDA
	bool updated = true;
#endif
public:
	string sky = "";
	Scene* scene;
	LightManager* lightMangager;
#ifdef CUDA
	Scene* dScene = nullptr;
#endif

	Camera cam;

	CUDA_H Renderer(RenderType rType = RenderType::LIT, int samplingRate = 4)
	{
		scene = /*new Scene();*/ new BVHTree();

		cam = Camera(CameraParam());
		lightMangager = new LightManager(scene);
		this->samplingRate = samplingRate;
		this->type = rType;
	}
	CUDA_H ~Renderer()
	{
		delete scene;
#ifdef CUDA
		if (dScene)
		{
			cudaFree(dScene);
		}
#endif
	}

private:
public:

	//inline TextureMap * getMapByName(string name) { return mapManager.getTextureByName(name); }

	void initScene(vector<BasicShape*> bsp) { scene->initScene(bsp); }

	CUDA_HD float traceRay(Ray initial, int32_t& fId, int colId, int start, int maxDepth, ExtraInfo info);

	void setSamplingRate(int samplingRate) {
		this->samplingRate = samplingRate;
	}
	void setRenderType(RenderType type)
	{
		this->type = type;
	}

	void CPURenderPPMImage(uint8_t **image, int32_t **ids, size_t width, size_t height, int depth);

	void CPURenderBlock(size_t width, size_t height, int32_t blockId, int32_t blocksCount, int depth);
 
	float gatherColorEnergy(Ray ray, float energy, int depths, int id, int& fId, float val, ExtraInfo info);

	void printScene() { scene->print(); };
#ifdef CUDA
	CUDA_H void CUDARenderPPMImage(uint8_t **image, int32_t **ids, size_t width, size_t height);
	__global__ void dRender(uint8_t * image, int32_t *ids, size_t width, size_t height, Camera cam);
#endif

	static Renderer& getInstance()
	{
		static Renderer renderer;
		return renderer;
	}

};
#endif