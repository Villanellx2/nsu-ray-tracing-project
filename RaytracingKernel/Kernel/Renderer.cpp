#include "./Renderer.h"

void Renderer::CPURenderBlock(size_t width, size_t height, int32_t blockId, int32_t blocksCount, int depth)
{
	size_t id = 0;
	//size_t hWindow = (height + blocksCount - 1) / blocksCount;
	//pragma for use OpenMP parallelizm
	//#pragma omp parallel for 
	for (size_t idx = blockId; idx < /*hWindow*/height; idx+=blocksCount)
	{
		//size_t i = hWindow * blockId + idx;
		size_t i = idx;
		if (i >= height) break;
		for (size_t j = 0; j < width; j++)
		{
			id = 3 * (i * width + j);
			(*ids)[id / 3] = -1;
			Vector3 color = Vector3(0, 0, 0);
			for (int x = 0; x < samplingRate; x++)
			{
				for (int y = 0; y < samplingRate; y++)
				{
					float dy = (i * 1.0f * samplingRate + x) / (samplingRate * height);
					float dx = (j * 1.0f * samplingRate + y) / (samplingRate * width);

					Ray r = Ray::makeRay(Vector3(), Vector3());
					int cid = -1; 
					cam.dirAtPoint(r.start, r.dir, 1 - dy, dx);
					ExtraInfo info = {-1, 0, 0};
					color.e[0] += traceRay(r, cid, 0, 0, depth, info);
					color.e[1] += traceRay(r, cid, 1, 0, depth, info);
					color.e[2] += traceRay(r, cid, 2, 0, depth, info);

					/*if (cid != -1)
					{
						(*ids)[id / 3] = cid;
					}*/
				}
			}
			color /= (float)samplingRate * samplingRate;
			(*image)[id] = uint8_t(255.5 * color.e[0]);
			(*image)[id + 1] = uint8_t(255.5 * color.e[1]);
			(*image)[id + 2] = uint8_t(255.5 * color.e[2]);
		}
		//std::cerr << "+1" << std::endl;
	}
}


void Renderer::CPURenderPPMImage(uint8_t** nimage, int32_t **nids, size_t width, size_t height, int depth)
{	
	skyMap = NULL;
	if (sky != "") skyMap = MapManager::getInstance().getTextureByName(sky);
	clock_t t1 = clock();
	size_t id = 0;

	image = nimage;
	ids = nids;

	int n = std::thread::hardware_concurrency();

	//do it if use openMP
	//n = 1;
	vector<std::thread> threads(n);

	for (int i = 0; i < n; i++)
	{
		threads[i] = std::thread(&Renderer::CPURenderBlock, this, width, height, i, n, depth);
	}
	for (auto& w : threads) w.join();

	clock_t t2 = clock();
	std::cerr << "Time to render: " << (t2 - t1) * 1.0 / CLOCKS_PER_SEC << std::endl;
}
#ifdef CUDA
CUDA_H void Renderer::CUDARenderPPMImage(uint8_t ** image, int32_t **ids, size_t width, size_t height)
{
	if (!updated)
	{
		if (gScene != nullptr)
		{
			gScene->clear();
			cudaFree(gScene);
		}
		cudaMalloc(&gScene, scene->getSize());
		cudaMemcpy(&gScene, scene, scene->getSize(), cudaMemcpyHostToDevice);
		scene->moveToCUDA(&gScene);
	}


	//dRender<<
}
__global__ void Renderer::dRender(uint8_t * image, int32_t * ids, size_t width, size_t height, Camera cam)
{
	/*int i = threadIdx.x + blockIdx.x * blockDim.x;
	int j = threadIdx.y + blockIdx.y * blockDim.y;
	if (i >= cam.param.Height || j >= cam.param.Width) return;
	int id = (i * cam.param.Width + j);

	float u = float(i) / float(cam.param.Height);
	float v = float(j) / float(cam.param.Width);

	Ray r = Ray(Vector3(), Vector3());
	cam.dirAtPoint(r.Start, r.Dir, u, v);

	image[id * 3] = TraceRay(r, ids[id], 0, -1, 5);
	image[id * 3 + 1] = TraceRay(r, ids[id], 1, -1, 5);
	image[id * 3 + 2] = TraceRay(r, ids[id], 2, -1, 5);*/
}
#endif

float Renderer::gatherColorEnergy(Ray ray, float energy, int depth, int id, int& fId, float val = 1, ExtraInfo info = {-1})
{
	float res = 0;
	HitResult hit;
	if (depth == 0 || energy < 1e-4  || !scene->castRay(hit, ray, id, info))
	{
		Vector3 dir = ray.direction();
		if (!skyMap)
		{
			float t = 0.5f * (dir.y() + 1.0f);
			return (Vector3(1.0f, 1.0f, 1.0f) * (1.0f - t) + Vector3(0.5f, 0.7f, 1.0f) * t).e[id] * energy;
		}
		else
		{
			float u = atan2f(dir.y(), dir.x()) / (PI2) + 0.5f;
			float v = dir.z() / 2 + 0.5f;
			return energy * skyMap->colorByUV(v, u, id);
		}
	//	return 0;
	}
	else
	{
		if (hit.nNormal * ray.dir >= 0) hit.nNormal *= -1;
		hit.nPos += hit.nNormal * BIAS;
		BasicShape* ht = scene->getShapeByID(hit.id);
		SurfaceProps props = ht->getMaterial()->getPropsByUV(hit.u, hit.v, id);
		LightInfo lInfo;
		/*float directLight = 1;
		float specularLight = 0;
		float globalIllumination = 0;*/
		if (type == RenderType::LIT)
		{
			lightMangager->calculateLight(lInfo, hit.nPos, hit.nNormal, props.specularRate, id, depth - 1, info);
		}
		float diffuseLight = lInfo.directLight + lInfo.globalIllumination;
		
		res += (props.color * props.diffuseRate * diffuseLight + props.smoothness * lInfo.specularLight) * energy;
		
		float n1 = val;
		float n2 = props.density;
		if (abs(n1 - n2) < EPS) n2 = 1;
		Vector3 refr_dir = Vector3::refract(ray.dir, hit.nNormal, n1, n2);
		Ray refracted = Ray::makeRay(hit.nPos - hit.nNormal * 2 * BIAS, refr_dir);
		Ray reflected = Ray::makeRay(hit.nPos, Vector3::reflect(ray.dir, hit.nNormal));
		if (refr_dir.d2() > 1.5f)
		{
			res += gatherColorEnergy(reflected, energy * (props.reflectionRate + props.refractionRate), depth - 1, id, fId, val, info);
		}
		else
		{
			res += gatherColorEnergy(refracted, energy * props.refractionRate, depth - 1, id, fId, n2, info);
			res += gatherColorEnergy(reflected, energy * props.reflectionRate, depth - 1, id, fId, val, info);
		}
		fId = info.info[0];
		return res;
	}
}

CUDA_HD float Renderer::traceRay(Ray initial, int32_t& fId, int colId, int start, int maxDepth, ExtraInfo info)
{
	/*float res = 0;
	float value = 1;

	HitResult hit;

	Ray current = initial;
	fId = -1;
	if (!scene->CastRay(hit, current, colId))
	{
		Vector3 dir = current.direction();
		float t = 0.5f * (dir.y() + 1.0f);
		return (Vector3(1.0f, 1.0f, 1.0f) * (1.0f - t) + Vector3(0.5f, 0.7f, 1.0f) * t).e[colId];
	}
	fId = hit.id;
	BasicShape* ht = scene->getShapeByID(hit.id);

	res += hit.col * ht->Mat.GetDiffuse();

	value *= ht->Mat.GetReflect();

	for (int i = 1; i < maxDepth; i++)
	{
		current.Start = hit.nPos + hit.nNormal * 0.02f;
		current.Dir -= current.Dir * hit.nNormal * 2.0f;
		if (value < 0.05f)
		{
			break;
		}
		if (scene->CastRay(hit, current, colId))
		{
			ht = scene->getShapeByID(hit.id);
			res += hit.col * ht->Mat.GetDiffuse();
			value *= ht->Mat.GetReflect();
		}
		else
		{
			break;
		}
	}
	return res;*/
	float v = gatherColorEnergy(initial, 1, maxDepth, colId, fId, 1 , info);
//	std::cerr << 1.0f << " " << v << " " << min(1.0f, v) << std::endl;
	return min(1.0f, v);
}