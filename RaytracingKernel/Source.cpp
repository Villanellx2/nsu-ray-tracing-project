#define _CRT_SECURE_NO_WARNINGS
#include "./Kernel/RayTracer.h"
#define Xc 1920
#define Yc 1080
#define N 20
int main()
{
	CameraParam param = CameraParam(4, 120.0f);
	// vector of shapes on scene
	vector<BasicShape*> shapes;
	// vector of point lights on scene
	vector<PointLight*> light;
	// vector of volumetric light sources on scene
	vector<VolumetricLight*> vl;
	Renderer& rd = Renderer::getInstance();
	//number of secondary rays used for global illumination gathering (better not to use)
	RandomGenerator& rg = RandomGenerator::getInstance();
	rg.setSeed((uint32_t)time(NULL));
	rd.lightMangager->secondaryRays = 0;
	//Light type UNLIT - no light gathering, LIT - gather light
	rd.setRenderType(RenderType::LIT);
	// how many rays per pixel are cast on each coord (3 per X coord and 3 per y - 9 rays per pixel)
	rd.setSamplingRate(2);
	// set skybox texture (color gathered by ray if it hasn't intersected shape)
	rd.sky = "sky1.ppm";

	// set camera position and properties
	rd.cam = Camera(param);
	rd.cam.setLocation(Vector3(3.5f, -10, 0.f));
	//rd.cam.rotate(Quaternion(Vector3(0, 0, 1), PI / 12));
	//rd.cam.rotate(Quaternion(rd.cam.X, -PI / 12));

	// clear old global illumination source and add pointer to new one 
	rd.lightMangager->setGlobalLight(new GlobalLight(Vector3(1, 1, 1), 0.1f));
	
	MapManager& manager = MapManager::getInstance();
	/**Scene 1: 2 spheres (red ruber-like, glass), 2 mirrors and cellular floor */ 
	
	shapes.push_back(new Sphere(1.0f, Vector3(0.f, 0.f, 0.f)));
	// red rubber material params: red color,
	// surface properties (diffusion coef, reflection coef, refraction coef)
	// roughness (specular intencity), material density (refraction index)
	//specular ratio (< 1 - there is no specular part, the closer rate to 1, the stronger specularity is (power coefficient of cosine in formula). 
	shapes.back()->setMaterial(Material(Vector3(1.f, .1f, .1f), Vector3(1.f, 0.f, 0.f), 0.5f, 1.6f, 45));
	
	shapes.push_back(new Sphere(1.0f, Vector3(3.f, 0.f, 0.f)));
	//glass material
	shapes.back()->setMaterial(Material(Vector3(0.1f, .1f, .1f),Vector3(0.1f, 0.4f, 0.5f), 0.2f, 1.6f, 4));
	
	shapes.push_back(new Plane(0, Vector3(0, 0, -2.5f), Quaternion(), Vector3(10, 8, 1)));
	
	shapes.back()->setMaterial(Material(Vector3(), Vector3(1.f, 0.f, 0.0f), 0.7f, 1.0));
	//apply texture
	shapes.back()->getMaterial()->setTextureMapName("tex2.ppm");

	shapes.push_back(new Plane(0, Vector3(-5.5f, 0, -2.5f), Quaternion(Vector3(0,1, 0), PI/2), Vector3(10, 8, 1)));
	shapes.back()->setMaterial(Material(Vector3(), Vector3(0.f, 1.f, 0.0f), 0.7f, 1.0));
	//apply texture
	//shapes.back()->getMaterial()->setTextureMapName("tex2.ppm");


	shapes.push_back(new Plane(0, Vector3(0, 2.5f, -2.5f), Quaternion(Vector3(1, 0, 0), PI / 2), Vector3(10, 8, 1)));
	shapes.back()->setMaterial(Material(Vector3(), Vector3(0.f, 1.f, 0.0f), 0.7f, 1.0));
	//apply texture
	shapes.back()->getMaterial()->setTextureMapName("tex2.ppm");
	


	//blue color point light  
	light.push_back(new PointLight(Vector3(0.f, 0.f, 1.f), 0.2f, Vector3(15, -5, 15)));
	// planar source of light
	// creates plane and assigns it to source, sampling rate of light (how many sources it generates during rendering), color of light and intencity
	vl.push_back(new PlaneLight(new Plane(0, Vector3(0, -5, 10), Quaternion(), Vector3(2.5, 2.5, 1)), N, Vector3(226.f/255, 152.f / 255, 34.f / 255), 0.8f));
	
	/**end of scene 1/
	/** scene 2: plane, sphere and 1 volumetric light sources*/
	/*
	shapes.push_back(new Plane(0, Vector3(0, 0, 0), Quaternion(Vector3(1, 0, 0), PI/2), Vector3(100, 100, 1)));
	shapes.back()->setMaterial(Material(Vector3(1.f,1.f,1.f), Vector3(1.f, 0.f, 0.0f), 0.f, 1.0));
	shapes.push_back(new Sphere(1.f, Vector3(0, 0, 0), Quaternion(Vector3(1, 0, 0), PI/2), Vector3(1.f, 1.f, 1.f)));
	shapes.back()->setMaterial(Material(Vector3(1.f, 1.f, 1.f), Vector3(1.f, 0.f, 0.0f), 0.f, 1.0));
	vl.push_back(new PlaneLight(new Plane(0, Vector3(-7.5f, -4.f, 0), Quaternion(Vector3(0, 1, 0), 3 * PI / 2), Vector3(2.f, 2.f, 1)), N, Vector3(1.f, 1.f, 1.f), 1.f));
	*/
	/* end of scene 2*/
	// set scene to render 
	rd.initScene(shapes);

	//set scene lights
	rd.lightMangager->setLights(light);
	rd.lightMangager->setVolumeLights(vl);
	//load textures
	manager.getTextureByName("sky1.ppm");
	for (auto v : shapes)
	{
		v->getMaterial()->loadMaps();
	}	

	freopen("out.ppm", "wb", stdout);
	uint8_t header[] = "P6 1920 1080 255\n";
	header[17] = 10;

	// Xc and Yc - defines to simplify debugging 
	uint8_t *image = (uint8_t*)malloc(Xc * Yc * 3 * sizeof(uint8_t));
	int32_t *ids = (int32_t*)malloc(Xc * Yc * sizeof(int32_t));
	
	rd.CPURenderPPMImage(&image, &ids, Xc, Yc, 3);
	rd.scene->printBenchmark();

	fwrite(header, sizeof(uint8_t), sizeof(header) - 1, stdout);
	fwrite(image, sizeof(uint8_t), Xc * Yc * 3, stdout);
	free(image);
	free(ids);
	return 0;
}