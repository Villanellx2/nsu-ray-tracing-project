#include "Render_UI.h"

Render_UI::Render_UI(QWidget* parent) : QMainWindow(parent)
{
	setWindowIcon(QIcon("icon.ico"));
	curr_dirr = new QString();
	ui.setupUi(this);
	
	ui.secondary->setHidden(true);
	ui.label_3->setHidden(true);

	connect(ui.open_button, &QPushButton::clicked, this, &Render_UI::browse);
	connect(ui.render_button, &QPushButton::clicked, this, &Render_UI::check);
	connect(ui.save_button, &QPushButton::clicked, this, &Render_UI::save);
	ui.res_1280_1024->setChecked(true);

	connect(ui.act0, &QAction::toggled, this, &Render_UI::change_projection);
	connect(ui.act1, &QAction::toggled, this, &Render_UI::change_projection);
	connect(ui.act2, &QAction::toggled, this, &Render_UI::change_projection);
	connect(ui.act3, &QAction::toggled, this, &Render_UI::change_projection);
	connect(ui.act4, &QAction::toggled, this, &Render_UI::change_projection);

	connect(ui.res_320_240, &QAction::toggled, this, &Render_UI::change_resolution);
	connect(ui.res_640_480, &QAction::toggled, this, &Render_UI::change_resolution);
	connect(ui.res_800_480, &QAction::toggled, this, &Render_UI::change_resolution);
	connect(ui.res_1024_768, &QAction::toggled, this, &Render_UI::change_resolution);
	connect(ui.res_1280_1024, &QAction::toggled, this, &Render_UI::change_resolution);

	connect(ui.action60, &QAction::toggled, this, &Render_UI::change_angle);
	connect(ui.action90, &QAction::toggled, this, &Render_UI::change_angle);
	connect(ui.action240, &QAction::toggled, this, &Render_UI::change_angle);
	connect(ui.action180, &QAction::toggled, this, &Render_UI::change_angle);
	connect(ui.action135, &QAction::toggled, this, &Render_UI::change_angle);
	connect(ui.action360, &QAction::toggled, this, &Render_UI::change_angle);
	connect(this, &Render_UI::rendered, this, &Render_UI::enable_buttons);

	QTextCodec::setCodecForLocale(QTextCodec::codecForName("CP 1251"));

	QPixmap myPixmap("hi.jpg");
	ui.image_lable->setPixmap(myPixmap);
	ui.image_lable->setScaledContents(true);
	ui.image_lable->setContentsMargins(0, 0, 0, 0);
	ui.image_lable->setStyleSheet("background-color:#d4d0d6");
}


void Render_UI::resizeEvent(QResizeEvent* event)
{
	Render_UI::resize_image();
}

void Render_UI::resize_image() {
	float d = (float)pred_w / (float)pred_h;
	int curr_w = this->size().width() - 135 - 50;
	int curr_h = this->size().height() - 150 - 10;
	int pref_h = curr_w / d;
	int pref_w = curr_h * d;
	QSize size;
	if (pref_h <= curr_h) {
		size = QSize(curr_w, pref_h);
		ui.image_lable->resize(size);
	}
	else {
		size = QSize(pref_w, curr_h);
		ui.image_lable->resize(size);
	}

	ui.message_label->setGeometry(12, this->size().height() - 141, curr_w + 135, 20);
	ui.pos_2->setGeometry(12, this->size().height() - 112, 79, 16);
	ui.label_4->setGeometry(363, this->size().height() - 112, 79, 16);
	ui.label_3->setGeometry(494, this->size().height() - 112, 79, 16);

	ui.pos->setGeometry(12, this->size().height() - 86, 79, 16);
	ui.label->setGeometry(363, this->size().height() - 86, 79, 16);
	ui.gatherLight->setGeometry(500, this->size().height() - 90, 90, 25);

	ui.pos_x->setGeometry(97, this->size().height() - 112, 58, 20);
	ui.pos_y->setGeometry(161, this->size().height() - 112, 58, 20);
	ui.pos_z->setGeometry(225, this->size().height() - 112, 58, 20);

	ui.rot_x->setGeometry(97, this->size().height() - 86, 58, 20);
	ui.rot_y->setGeometry(161, this->size().height() - 86, 58, 20);
	ui.rot_z->setGeometry(225, this->size().height() - 86, 58, 20);
	ui.rot->setGeometry(289, this->size().height() - 86, 58, 20);

	ui.secondary->setGeometry(576, this->size().height() - 112, 40, 20);
	ui.perPixel->setGeometry(448, this->size().height() - 112, 40, 20);
	ui.depth->setGeometry(448, this->size().height() - 86, 58, 20);
}
