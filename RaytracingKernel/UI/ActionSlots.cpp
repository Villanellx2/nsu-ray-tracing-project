#include "Render_UI.h"

void Render_UI::change_resolution() {
	if (changing) return;
	changing = true;
	if (ui.res_1024_768->isChecked() && h != 768) {
		set_resolution(1024, 768);
		changing = false;
		return;
	}
	if (ui.res_1280_1024->isChecked() && h != 1080) {
		set_resolution(1920, 1080);
		changing = false;
		return;
	}
	if (ui.res_800_480->isChecked() && (h != 480 || w != 800)) {
		set_resolution(800, 480);
		changing = false;
		return;
	}
	if (ui.res_640_480->isChecked() && (h != 480 || w != 640)) {
		set_resolution(640, 480);
		changing = false;
		return;
	}
	if (ui.res_320_240->isChecked() && h != 240) {
		set_resolution(320, 240);
		changing = false;
		return;
	}
}

void Render_UI::set_resolution(int w1, int h1) {
	h = h1;
	w = w1;
	ui.res_1280_1024->setChecked(false);
	ui.res_1024_768->setChecked(false);
	ui.res_800_480->setChecked(false);
	ui.res_640_480->setChecked(false);
	ui.res_320_240->setChecked(false);

	switch (w)
	{
	case 1920:
		ui.res_1280_1024->setChecked(true);
		break;
	case 1024:
		ui.res_1024_768->setChecked(true);
		break;
	case 800:
		ui.res_800_480->setChecked(true);
		break;
	case 640:
		ui.res_640_480->setChecked(true);
		break;
	case 320:
		ui.res_320_240->setChecked(true);
		break;
	}
	char buffer[50];
	sprintf(buffer, "set resolution %d * %d", h1, w1);
	ui.message_label->setText(buffer);

}

void Render_UI::change_angle() {
	if (changing) return;
	changing = true;

	if (ui.action60->isChecked() && cam_angle != 60) {
		set_angle(60);
		changing = false;
		return;
	}
	if (ui.action90->isChecked() && cam_angle != 90) {
		set_angle(90);
		changing = false;
		return;
	}
	if (ui.action135->isChecked() && cam_angle != 135) {
		set_angle(135);
		changing = false;
		return;
	}
	if (ui.action180->isChecked() && cam_angle != 180) {
		set_angle(180);
		changing = false;
		return;
	}
	if (ui.action240->isChecked() && cam_angle != 240) {
		set_angle(240);
		changing = false;
		return;
	}
	if (ui.action360->isChecked() && cam_angle != 360) {
		set_angle(360);
		changing = false;
		return;
	}
	set_angle(cam_angle);
	changing = false;
}

void Render_UI::set_angle(int angle) {
	cam_angle = angle;
	ui.action60->setChecked(false);
	ui.action90->setChecked(false);
	ui.action135->setChecked(false);
	ui.action180->setChecked(false);
	ui.action240->setChecked(false);
	ui.action360->setChecked(false);
	switch (angle)
	{
	case 45:
		ui.action60->setChecked(true);
		break;
	case 90:
		ui.action90->setChecked(true);
		break;
	case 135:
		ui.action135->setChecked(true);
		break;
	case 180:
		ui.action180->setChecked(true);
		break;
	case 240:
		ui.action240->setChecked(true);
		break;
	case 360:
		ui.action360->setChecked(true);
		break;

	}
	char buffer[50];
	sprintf(buffer, "set camera angle %d", angle);
	ui.message_label->setText(buffer);
}

void Render_UI::change_cam() {
	Vector3 pos = p->getCameraPosition();
	Quaternion rot = p->getCameraRotation();
	ui.pos_x->setValue(pos.x());
	ui.pos_y->setValue(pos.y());
	ui.pos_z->setValue(pos.z());
	ui.rot_x->setValue(rot.x());
	ui.rot_y->setValue(rot.y());
	ui.rot_z->setValue(rot.z());
	ui.rot->setValue(rot.w());
}

void Render_UI::change_projection() {
	if (changing)
		return;
	changing = true;

	if (ui.act0->isChecked() && projection != 0) {
		set_projection(0);
		changing = false;
		return;
	}
	if (ui.act1->isChecked() && projection != 1) {
		set_projection(1);
		changing = false;
		return;
	}
	if (ui.act2->isChecked() && projection != 2) {
		set_projection(2);
		changing = false;
		return;
	}
	if (ui.act3->isChecked() && projection != 3) {
		set_projection(3);
		changing = false;
		return;
	}
	if (ui.act4->isChecked() && projection != 4) {
		set_projection(4);
		changing = false;
		return;
	}

	set_projection(projection);
	changing = false;
}

void Render_UI::set_projection(int p) {
	ui.act0->setChecked(false);
	ui.act1->setChecked(false);
	ui.act2->setChecked(false);
	ui.act3->setChecked(false);
	ui.act4->setChecked(false);
	switch (p)
	{
	case 0:
		ui.act0->setChecked(true);
		projection = 0;
		break;
	case 1:
		ui.act1->setChecked(true);
		projection = 1;
		break;
	case 2:
		ui.act2->setChecked(true);
		projection = 2;
		break;
	case 3:
		ui.act3->setChecked(true);
		projection = 3;
		break;
	case 4:
		ui.act4->setChecked(true);
		projection = 4;
		break;
	}
	char buffer[50];
	sprintf(buffer, "projection setted");
	ui.message_label->setText(buffer);
}