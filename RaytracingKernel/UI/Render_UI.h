#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Render_UI.h"
#include "../Parser/x3dParser.h"
#include "qtextcodec.h"

class Render_UI : public QMainWindow
{
	Q_OBJECT

public:

	Render_UI(QWidget* parent = Q_NULLPTR);
	virtual void resizeEvent(QResizeEvent* event) override;

public slots:
	void browse();
	void check();
	void save();
	void start();
	void set_resolution(int w, int h);
	void set_angle(int angle);
	void change_angle();
	void change_projection();
	void set_projection(int p);
	void change_resolution();
	void change_cam();
	void enable_buttons();
	void resize_image();

signals:
	void rendered();
	//void resolution_changed();

private:
	Ui::Render_UIClass ui;
	int projection = 4;
	int w = 1920, h = 1080;
	int pred_w = 1920, pred_h = 1080;
	QString* curr_dirr;
	Parser* p = nullptr;
	uint8_t* image = nullptr;
	int32_t* ids = nullptr;
	bool changing = false;
	float cam_angle = 90.0f;
	Vector3 pos_d = Vector3();
	Vector3 camera_position;
	Quaternion camera_rotation;

};