#include "Render_UI.h"
#include "qtextstream.h"
#include "qthread.h"
#include "qfiledialog.h"
#include "qlineedit.h"
#include "qmessagebox.h"
#include "windows.h"

uint8_t* renderedImage = (uint8_t*)malloc(1);
clock_t t1;
clock_t t2;

void Render_UI::browse()
{
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("CP 1251"));
	QFileDialog* dialog = new QFileDialog();
	dialog->setStyleSheet("max-width: 700px;");

	QString dir = dialog->getOpenFileName(this, tr("Open File"), "/home", tr("*.x3d *.xml *.html")).toLocal8Bit();
	if (dir == nullptr) return;
	*curr_dirr = dir;
	p = new Parser(curr_dirr->toStdString());
	if (!(p->hasNext()))
	{
		p = nullptr;
		QMessageBox::critical(nullptr, "Incorrect file", "File is incorrect or we can't render this scene");
		ui.render_button->setEnabled(false);
		return;
	}
	ui.message_label->setText("Scene added");
	ui.render_button->setEnabled(true);
	change_cam();
	pos_d = Vector3();
}


void Render_UI::check()
{
	if (curr_dirr == nullptr) {
		QMessageBox::critical(nullptr, "Incorrect file", "Choose file with X3D scene");
		return;
	}
	if (p == nullptr && curr_dirr != nullptr)
		p = new Parser(curr_dirr->toStdString());

	if (!(p->hasNext()))
	{
		p = nullptr;
		QMessageBox::critical(nullptr, "Incorrect file", "File is incorrect or we can't render this scene");
		return;
	}

	ui.open_button->setEnabled(false);
	ui.render_button->setEnabled(false);
	QThread* render = QThread::create(&Render_UI::start, this);
	if (pred_w != w || pred_h != h) {
		pred_w = w;
		pred_h = h;
		ui.image_lable->setPixmap(QPixmap());
		resize_image();
	}

	render->start();
	ui.image_lable->setCursor(QCursor(Qt::BusyCursor));
}

void Render_UI::start()
{
	free(renderedImage);

	image = (uint8_t*)malloc(w * h * 3 * sizeof(uint8_t));
	ids = (int32_t*)malloc(w * h * sizeof(int32_t));

	std::vector<BasicShape*> shapes;
	while (p->hasNext()) shapes.push_back(p->NextElem());

	CameraParam param = CameraParam(projection, cam_angle);
	Renderer rd = Renderer();
	MapManager& manager = MapManager::getInstance();
	rd.initScene(shapes);

	if (p->getGlobalLight() != nullptr)
		rd.lightMangager->setGlobalLight(p->getGlobalLight());

	if (!p->getVolumetricLight().empty())
		rd.lightMangager->setVolumeLights(p->getVolumetricLight());

	if (!p->getSoursesOfLight().empty())
		rd.lightMangager->setLights(p->getSoursesOfLight());
	
	rd.lightMangager->secondaryRays = ui.secondary->value();
	if (ui.gatherLight->isChecked()) 
		rd.setRenderType(RenderType::LIT);
	else 
		rd.setRenderType(RenderType::UNLIT);

	rd.setSamplingRate(ui.perPixel->value());

	if (p->hasSky()) {
		rd.sky = p->getSky();
		manager.getTextureByName(rd.sky);
	}

	for (auto v : shapes) v->getMaterial()->loadMaps();

	rd.cam = Camera(param);
	rd.cam.setLocation(Vector3(ui.pos_x->value(), ui.pos_y->value(), ui.pos_z->value()));
	rd.cam.rotate(Quaternion(Vector3(ui.rot_x->value(), ui.rot_y->value(), ui.rot_z->value()), ui.rot->value()));
	RandomGenerator& rg = RandomGenerator::getInstance();
	rg.setSeed(time(NULL));

	ui.message_label->setText("start rendering...");

	t1 = clock();

	renderedImage = (uint8_t*)malloc((11 + (int)log10(h) + (int)log10(w) + w * h * 3) * (sizeof(char)));
	image = &renderedImage[11 + (int)log10(h) + (int)log10(w)];

	snprintf((char*)renderedImage, (11 + (int)log10(h) + (int)log10(w)), "P6 %d %d 255\n", w, h);
	int depth = ui.depth->value();

	rd.CPURenderPPMImage(&image, &ids, w, h, depth);

	t2 = clock();

	rd.scene->printBenchmark();
	this->rendered();
	free(ids);
	free(p);
	p = nullptr;
}

void Render_UI::enable_buttons() {
	ui.open_button->setEnabled(true);
	ui.render_button->setEnabled(true);
	ui.save_button->setEnabled(true);

	char* message = (char*)malloc(sizeof(char) * 44 +  sizeof("Rendering time = "));
	sprintf(message, "Rendering time = %f s", (t2 - t1) * 1.0 / CLOCKS_PER_SEC);
	ui.message_label->setText(QString(message));
	ui.image_lable->setCursor(QCursor(Qt::ArrowCursor));
	QPixmap myPixmap;
	myPixmap.loadFromData(QByteArray((char*)renderedImage, (11 + (int)log10(h) + (int)log10(w) + w * h * 3)));
	ui.image_lable->setPixmap(myPixmap);
}

void Render_UI::save()
{
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("CP 1251"));
	QString fileName = QFileDialog::getSaveFileName(this,
		tr("Save file"), "",
		("*.ppm"));

	if (fileName != "") {
		QFile file(fileName);
		if (!file.open(QIODevice::WriteOnly)) {
			QMessageBox msgBox; msgBox.setText("�� ���� �������� ����"); msgBox.exec();
		}
		else {
			char* file_name = (char*)malloc(sizeof(char) * (fileName.toStdString().length() + 4 + 1));
			int len = fileName.toStdString().length() + 1;
			strcpy(file_name, fileName.toLocal8Bit().toStdString().c_str());

			freopen(file_name, "wb", stdout);

			uint8_t* header = (uint8_t*)malloc((12 + log10(h) + log10(w)) * (sizeof(char)));
			snprintf((char*)header, (12 + (int)log10(h) + (int)log10(w)), "P6 %d %d 255\n\n", w, h);

			fwrite(header, sizeof(uint8_t), (11 + (int)log10(h) + (int)log10(w)), stdout);
			fwrite(image, sizeof(uint8_t), w * h * 3, stdout);
		}
	}
	ui.message_label->setText("file saved");
}