#ifndef UI_RENDER_UI_H
#define UI_RENDER_UI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Render_UIClass
{
public:
    QAction *res_320_240;
    QAction *res_640_360;
    QAction *res_640_480;
    QAction *res_800_480;
    QAction *res_1024_768;
    QAction *res_1280_1024;
    QAction *action60;
    QAction *action90;
    QAction *action135;
    QAction *action180;
    QAction *action240;
    QAction *action360;
    QAction *actionCamera_settings;
    QAction *act0;
    QAction *act1;
    QAction *act2;
    QAction *act3;
    QAction *act4;
    QWidget *centralWidget;
    QLabel *image_lable;
    QLabel *message_label;
    QWidget *widget;
    QVBoxLayout *verticalLayout_2;
    QPushButton *open_button;
    QPushButton *render_button;
    QPushButton *save_button;
    QSpacerItem *verticalSpacer;
    QLabel *pos_2;
    QDoubleSpinBox *pos_x;
    QDoubleSpinBox *pos_z;
    QDoubleSpinBox *pos_y;
    QDoubleSpinBox *rot_x;
    QLabel *pos;
    QDoubleSpinBox *rot_y;
    QDoubleSpinBox *rot_z;
    QDoubleSpinBox *rot;
    QSpinBox *secondary;
    QCheckBox *gatherLight;
    QLabel *label_4;
    QLabel *label_3;
    QSpinBox *depth;
    QSpinBox *perPixel;
    QLabel *label;
    QMenuBar *menuBar;
    QMenu *menux3d_render;
    QMenu *menuImage_resolution;
    QMenu *menuViewing_angle;
    QMenu *menuProjection;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Render_UIClass)
    {
        if (Render_UIClass->objectName().isEmpty())
            Render_UIClass->setObjectName(QString::fromUtf8("Render_UIClass"));
        Render_UIClass->setWindowModality(Qt::ApplicationModal);
        Render_UIClass->resize(702, 461);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(Render_UIClass->sizePolicy().hasHeightForWidth());
        Render_UIClass->setSizePolicy(sizePolicy);
        Render_UIClass->setMinimumSize(QSize(702, 461));
        Render_UIClass->setMaximumSize(QSize(16777215, 16777215));
        Render_UIClass->setSizeIncrement(QSize(1, 1));
        Render_UIClass->setBaseSize(QSize(800, 600));
        QPalette palette;
        QBrush brush(QColor(55, 84, 51, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(228, 240, 221, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(227, 244, 225, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush2);
        QBrush brush3(QColor(72, 84, 70, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush3);
        QBrush brush4(QColor(28, 43, 26, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Text, brush4);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush4);
        QBrush brush5(QColor(250, 250, 250, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush5);
        QBrush brush6(QColor(249, 249, 249, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush6);
        QBrush brush7(QColor(55, 84, 51, 128));
        brush7.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush7);
#endif
        Render_UIClass->setPalette(palette);
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        Render_UIClass->setFont(font);
        res_320_240 = new QAction(Render_UIClass);
        res_320_240->setObjectName(QString::fromUtf8("res_320_240"));
        res_320_240->setCheckable(true);
        res_640_360 = new QAction(Render_UIClass);
        res_640_360->setObjectName(QString::fromUtf8("res_640_360"));
        res_640_360->setCheckable(true);
        res_640_480 = new QAction(Render_UIClass);
        res_640_480->setObjectName(QString::fromUtf8("res_640_480"));
        res_640_480->setCheckable(true);
        res_800_480 = new QAction(Render_UIClass);
        res_800_480->setObjectName(QString::fromUtf8("res_800_480"));
        res_800_480->setCheckable(true);
        res_1024_768 = new QAction(Render_UIClass);
        res_1024_768->setObjectName(QString::fromUtf8("res_1024_768"));
        res_1024_768->setCheckable(true);
        res_1280_1024 = new QAction(Render_UIClass);
        res_1280_1024->setObjectName(QString::fromUtf8("res_1280_1024"));
        res_1280_1024->setCheckable(true);
        res_1280_1024->setChecked(true);
        action60 = new QAction(Render_UIClass);
        action60->setObjectName(QString::fromUtf8("action60"));
        action60->setCheckable(true);
        action90 = new QAction(Render_UIClass);
        action90->setObjectName(QString::fromUtf8("action90"));
        action90->setCheckable(true);
        action90->setChecked(true);
        action135 = new QAction(Render_UIClass);
        action135->setObjectName(QString::fromUtf8("action135"));
        action135->setCheckable(true);
        action180 = new QAction(Render_UIClass);
        action180->setObjectName(QString::fromUtf8("action180"));
        action180->setCheckable(true);
        action240 = new QAction(Render_UIClass);
        action240->setObjectName(QString::fromUtf8("action240"));
        action240->setCheckable(true);
        action240->setChecked(false);
        action360 = new QAction(Render_UIClass);
        action360->setObjectName(QString::fromUtf8("action360"));
        action360->setCheckable(true);
        actionCamera_settings = new QAction(Render_UIClass);
        actionCamera_settings->setObjectName(QString::fromUtf8("actionCamera_settings"));
        act0 = new QAction(Render_UIClass);
        act0->setObjectName(QString::fromUtf8("act0"));
        act0->setCheckable(true);
        act1 = new QAction(Render_UIClass);
        act1->setObjectName(QString::fromUtf8("act1"));
        act1->setCheckable(true);
        act2 = new QAction(Render_UIClass);
        act2->setObjectName(QString::fromUtf8("act2"));
        act2->setCheckable(true);
        act3 = new QAction(Render_UIClass);
        act3->setObjectName(QString::fromUtf8("act3"));
        act3->setCheckable(true);
        act4 = new QAction(Render_UIClass);
        act4->setObjectName(QString::fromUtf8("act4"));
        act4->setCheckable(true);
        act4->setChecked(true);
        centralWidget = new QWidget(Render_UIClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        image_lable = new QLabel(centralWidget);
        image_lable->setObjectName(QString::fromUtf8("image_lable"));
        image_lable->setEnabled(true);
        image_lable->setGeometry(QRect(150, 10, 521, 292));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(image_lable->sizePolicy().hasHeightForWidth());
        image_lable->setSizePolicy(sizePolicy1);
        image_lable->setMinimumSize(QSize(389, 292));
        image_lable->setMaximumSize(QSize(16777215, 16777215));
        image_lable->setSizeIncrement(QSize(1, 1));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Arial"));
        font1.setPointSize(8);
        image_lable->setFont(font1);
        message_label = new QLabel(centralWidget);
        message_label->setObjectName(QString::fromUtf8("message_label"));
        message_label->setGeometry(QRect(10, 321, 661, 20));
        message_label->setMinimumSize(QSize(0, 20));
        message_label->setMaximumSize(QSize(16777215, 40));
        message_label->setFont(font1);
        widget = new QWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(9, 9, 132, 307));
        verticalLayout_2 = new QVBoxLayout(widget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        open_button = new QPushButton(widget);
        open_button->setObjectName(QString::fromUtf8("open_button"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(open_button->sizePolicy().hasHeightForWidth());
        open_button->setSizePolicy(sizePolicy2);
        open_button->setMinimumSize(QSize(130, 25));
        open_button->setMaximumSize(QSize(130, 16777215));
        open_button->setFont(font1);
        open_button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(open_button);

        render_button = new QPushButton(widget);
        render_button->setObjectName(QString::fromUtf8("render_button"));
        render_button->setEnabled(false);
        render_button->setMinimumSize(QSize(130, 25));
        render_button->setMaximumSize(QSize(130, 16777215));
        render_button->setSizeIncrement(QSize(0, 0));
        render_button->setFont(font1);
        render_button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(render_button);

        save_button = new QPushButton(widget);
        save_button->setObjectName(QString::fromUtf8("save_button"));
        save_button->setEnabled(false);
        sizePolicy2.setHeightForWidth(save_button->sizePolicy().hasHeightForWidth());
        save_button->setSizePolicy(sizePolicy2);
        save_button->setMinimumSize(QSize(130, 25));
        save_button->setMaximumSize(QSize(130, 16777215));
        save_button->setFont(font1);
        save_button->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout_2->addWidget(save_button);

        verticalSpacer = new QSpacerItem(20, 212, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        pos_2 = new QLabel(centralWidget);
        pos_2->setObjectName(QString::fromUtf8("pos_2"));
        pos_2->setEnabled(true);
        pos_2->setGeometry(QRect(12, 349, 79, 16));
        pos_2->setFont(font1);
        pos_x = new QDoubleSpinBox(centralWidget);
        pos_x->setObjectName(QString::fromUtf8("pos_x"));
        pos_x->setEnabled(true);
        pos_x->setGeometry(QRect(97, 349, 58, 20));
        pos_x->setMinimumSize(QSize(40, 20));
        pos_x->setFont(font1);
        pos_x->setDecimals(2);
        pos_x->setMinimum(-100.000000000000000);
        pos_x->setMaximum(100.989999999999995);
        pos_z = new QDoubleSpinBox(centralWidget);
        pos_z->setObjectName(QString::fromUtf8("pos_z"));
        pos_z->setEnabled(true);
        pos_z->setGeometry(QRect(225, 349, 58, 20));
        pos_z->setMinimumSize(QSize(40, 20));
        pos_z->setFont(font1);
        pos_z->setDecimals(2);
        pos_z->setMinimum(-100.000000000000000);
        pos_z->setMaximum(100.989999999999995);
        pos_y = new QDoubleSpinBox(centralWidget);
        pos_y->setObjectName(QString::fromUtf8("pos_y"));
        pos_y->setEnabled(true);
        pos_y->setGeometry(QRect(161, 349, 58, 20));
        pos_y->setMinimumSize(QSize(40, 20));
        pos_y->setFont(font1);
        pos_y->setDecimals(2);
        pos_y->setMinimum(-100.000000000000000);
        pos_y->setMaximum(100.989999999999995);
        rot_x = new QDoubleSpinBox(centralWidget);
        rot_x->setObjectName(QString::fromUtf8("rot_x"));
        rot_x->setEnabled(true);
        rot_x->setGeometry(QRect(97, 375, 51, 20));
        rot_x->setMinimumSize(QSize(40, 20));
        rot_x->setFont(font1);
        rot_x->setDecimals(2);
        rot_x->setMinimum(-1.000000000000000);
        rot_x->setMaximum(1.000000000000000);
        rot_x->setSingleStep(0.100000000000000);
        pos = new QLabel(centralWidget);
        pos->setObjectName(QString::fromUtf8("pos"));
        pos->setEnabled(true);
        pos->setGeometry(QRect(12, 375, 78, 16));
        pos->setFont(font1);
        rot_y = new QDoubleSpinBox(centralWidget);
        rot_y->setObjectName(QString::fromUtf8("rot_y"));
        rot_y->setEnabled(true);
        rot_y->setGeometry(QRect(161, 375, 46, 20));
        rot_y->setMinimumSize(QSize(40, 20));
        rot_y->setFont(font1);
        rot_y->setDecimals(2);
        rot_y->setMinimum(-1.000000000000000);
        rot_y->setMaximum(1.000000000000000);
        rot_y->setSingleStep(0.100000000000000);
        rot_z = new QDoubleSpinBox(centralWidget);
        rot_z->setObjectName(QString::fromUtf8("rot_z"));
        rot_z->setEnabled(true);
        rot_z->setGeometry(QRect(225, 375, 46, 20));
        rot_z->setMinimumSize(QSize(40, 20));
        rot_z->setFont(font1);
        rot_z->setDecimals(2);
        rot_z->setMinimum(-1.000000000000000);
        rot_z->setMaximum(1.000000000000000);
        rot_z->setSingleStep(0.100000000000000);
        rot_z->setValue(0.000000000000000);
        rot = new QDoubleSpinBox(centralWidget);
        rot->setObjectName(QString::fromUtf8("rot"));
        rot->setEnabled(true);
        rot->setGeometry(QRect(289, 375, 58, 20));
        sizePolicy2.setHeightForWidth(rot->sizePolicy().hasHeightForWidth());
        rot->setSizePolicy(sizePolicy2);
        rot->setMinimumSize(QSize(40, 20));
        rot->setFont(font1);
        rot->setDecimals(2);
        rot->setMinimum(-100.000000000000000);
        rot->setMaximum(100.989999999999995);
        secondary = new QSpinBox(centralWidget);
        secondary->setObjectName(QString::fromUtf8("secondary"));
        secondary->setGeometry(QRect(448, 349, 40, 20));
        secondary->setMinimumSize(QSize(40, 20));
        secondary->setMaximumSize(QSize(40, 20));
        secondary->setFont(font1);
        secondary->setMinimum(0);
        secondary->setMaximum(1000);
        secondary->setValue(0);
        gatherLight = new QCheckBox(centralWidget);
        gatherLight->setObjectName(QString::fromUtf8("gatherLight"));
        gatherLight->setGeometry(QRect(494, 376, 76, 18));
        gatherLight->setFont(font1);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(494, 349, 65, 16));
        label_4->setMaximumSize(QSize(16777215, 20));
        label_4->setFont(font1);
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(363, 349, 78, 16));
        label_3->setMaximumSize(QSize(16777215, 20));
        label_3->setFont(font1);
        depth = new QSpinBox(centralWidget);
        depth->setObjectName(QString::fromUtf8("depth"));
        depth->setGeometry(QRect(448, 375, 40, 20));
        depth->setMinimumSize(QSize(40, 20));
        depth->setMaximumSize(QSize(40, 20));
        depth->setFont(font1);
        depth->setMinimum(1);
        depth->setMaximum(80);
        depth->setValue(4);
        perPixel = new QSpinBox(centralWidget);
        perPixel->setObjectName(QString::fromUtf8("perPixel"));
        perPixel->setGeometry(QRect(576, 349, 40, 20));
        perPixel->setMinimumSize(QSize(40, 20));
        perPixel->setMaximumSize(QSize(40, 20));
        perPixel->setFont(font1);
        perPixel->setMinimum(0);
        perPixel->setMaximum(1000);
        perPixel->setValue(3);
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(363, 375, 79, 16));
        label->setMaximumSize(QSize(16777215, 20));
        label->setFont(font1);
        Render_UIClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Render_UIClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 702, 21));
        menux3d_render = new QMenu(menuBar);
        menux3d_render->setObjectName(QString::fromUtf8("menux3d_render"));
        menuImage_resolution = new QMenu(menux3d_render);
        menuImage_resolution->setObjectName(QString::fromUtf8("menuImage_resolution"));
        menuViewing_angle = new QMenu(menux3d_render);
        menuViewing_angle->setObjectName(QString::fromUtf8("menuViewing_angle"));
        menuProjection = new QMenu(menux3d_render);
        menuProjection->setObjectName(QString::fromUtf8("menuProjection"));
        Render_UIClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Render_UIClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        Render_UIClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Render_UIClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Render_UIClass->setStatusBar(statusBar);

        menuBar->addAction(menux3d_render->menuAction());
        menux3d_render->addAction(menuImage_resolution->menuAction());
        menux3d_render->addAction(menuViewing_angle->menuAction());
        menux3d_render->addAction(menuProjection->menuAction());
        menuImage_resolution->addAction(res_320_240);
        menuImage_resolution->addAction(res_640_480);
        menuImage_resolution->addAction(res_800_480);
        menuImage_resolution->addAction(res_1024_768);
        menuImage_resolution->addAction(res_1280_1024);
        menuViewing_angle->addAction(action60);
        menuViewing_angle->addAction(action90);
        menuViewing_angle->addAction(action135);
        menuViewing_angle->addAction(action180);
        menuViewing_angle->addAction(action240);
        menuViewing_angle->addAction(action360);
        menuProjection->addAction(act0);
        menuProjection->addAction(act1);
        menuProjection->addAction(act2);
        menuProjection->addAction(act3);
        menuProjection->addAction(act4);
        mainToolBar->addSeparator();

        retranslateUi(Render_UIClass);

        QMetaObject::connectSlotsByName(Render_UIClass);
    } // setupUi

    void retranslateUi(QMainWindow *Render_UIClass)
    {
        Render_UIClass->setWindowTitle(QCoreApplication::translate("Render_UIClass", "Ray-tracer ", nullptr));
        res_320_240->setText(QCoreApplication::translate("Render_UIClass", "320\303\227240", nullptr));
        res_640_360->setText(QCoreApplication::translate("Render_UIClass", "640\303\227360", nullptr));
        res_640_480->setText(QCoreApplication::translate("Render_UIClass", "640\303\227480", nullptr));
        res_800_480->setText(QCoreApplication::translate("Render_UIClass", "800\303\227480", nullptr));
        res_1024_768->setText(QCoreApplication::translate("Render_UIClass", "1024\303\227768", nullptr));
        res_1280_1024->setText(QCoreApplication::translate("Render_UIClass", "1920\303\2271080", nullptr));
        action60->setText(QCoreApplication::translate("Render_UIClass", "60\302\260", nullptr));
        action90->setText(QCoreApplication::translate("Render_UIClass", "90\302\260", nullptr));
        action135->setText(QCoreApplication::translate("Render_UIClass", "135\302\260", nullptr));
        action180->setText(QCoreApplication::translate("Render_UIClass", "180\302\260", nullptr));
        action240->setText(QCoreApplication::translate("Render_UIClass", "240\302\260", nullptr));
        action360->setText(QCoreApplication::translate("Render_UIClass", "360\302\260", nullptr));
        actionCamera_settings->setText(QCoreApplication::translate("Render_UIClass", "Camera settings", nullptr));
        act0->setText(QCoreApplication::translate("Render_UIClass", "Perspective projection", nullptr));
        act1->setText(QCoreApplication::translate("Render_UIClass", "Infinity focus", nullptr));
        act2->setText(QCoreApplication::translate("Render_UIClass", "Panini projection", nullptr));
        act3->setText(QCoreApplication::translate("Render_UIClass", "Cylinder", nullptr));
        act4->setText(QCoreApplication::translate("Render_UIClass", "Sphere", nullptr));
        image_lable->setText(QString());
        message_label->setText(QString());
        open_button->setText(QCoreApplication::translate("Render_UIClass", "Open", nullptr));
        render_button->setText(QCoreApplication::translate("Render_UIClass", "Render", nullptr));
        save_button->setText(QCoreApplication::translate("Render_UIClass", "Save", nullptr));
        pos_2->setText(QCoreApplication::translate("Render_UIClass", "Camera position:", nullptr));
        pos->setText(QCoreApplication::translate("Render_UIClass", "Camera rotation:", nullptr));
        gatherLight->setText(QCoreApplication::translate("Render_UIClass", "LIT \\ UNLIT", nullptr));
        label_4->setText(QCoreApplication::translate("Render_UIClass", "Sampling rate", nullptr));
        label_3->setText(QCoreApplication::translate("Render_UIClass", "Secondary rays", nullptr));
        label->setText(QCoreApplication::translate("Render_UIClass", "Rendering depth", nullptr));
        menux3d_render->setTitle(QCoreApplication::translate("Render_UIClass", "Render", nullptr));
        menuImage_resolution->setTitle(QCoreApplication::translate("Render_UIClass", "Image resolution", nullptr));
        menuViewing_angle->setTitle(QCoreApplication::translate("Render_UIClass", "Viewing angle", nullptr));
        menuProjection->setTitle(QCoreApplication::translate("Render_UIClass", "Projection", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Render_UIClass: public Ui_Render_UIClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RENDER_UI_H
